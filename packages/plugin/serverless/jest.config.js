module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleNameMapper: {
    '^~/(.*)$': '<rootDir>/src/$1',
  },
  reporters: [
    'default',
    [
      '../../../node_modules/jest-html-reporter',
      {
        pageTitle: 'Powtoon AppSync Serverless Test Report',
        outputPath: 'coverage/index.html',
      },
    ],
  ],
};
