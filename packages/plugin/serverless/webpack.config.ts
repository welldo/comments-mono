/* eslint-disable @typescript-eslint/no-use-before-define */
import * as path from 'path';
import { lstatSync, readdirSync } from 'fs';
import { basename, extname } from 'path';

import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
// import { IgnorePlugin } from 'webpack';

// const validationParameters = ['Input', 'Output'];
const posixPath = (path: string) => path.replace(/\\/g, '/');
const resolve = (...args: string[]) => posixPath(path.resolve(...args));
const srcDir = resolve(__dirname, 'src');
const outDir = resolve(__dirname, 'build');
const lambdasSrcDir = resolve(srcDir, 'lambdas');

module.exports = () => {
  const plugins: any[] = [
    new CopyWebpackPlugin({
      patterns: [
        {
          from: '**/*.(gql|json)',
          context: path.resolve('./', 'src'),
        },
      ],
    }),
    new CleanWebpackPlugin(),
  ];
  const entries: any = {
    ...getLambdasForDirectory(lambdasSrcDir),
  };

  return {
    mode: 'production',
    target: 'node',
    entry: entries,
    devtool: 'source-map',
    optimization: {
      minimize: false,
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /(node_modules|(\.spec\.ts))/,
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.js'],
      mainFields: ['main'],
      alias: {
        '~': [srcDir],
      },
    },
    externals: ['aws-sdk'],
    output: {
      libraryTarget: 'commonjs',
      filename: '[name]/index.js',
      path: outDir,
    },
    plugins: plugins,
  };
};

const getLambdasForDirectory = (lambdasDir: any) => {
  return readdirSync(lambdasDir)
    .flatMap(categoryDir => {
      return readdirSync(resolve(lambdasDir, categoryDir)).map(dirOrFileItem => {
        const path = resolve(lambdasDir, categoryDir, dirOrFileItem);
        const file = lstatSync(path).isFile() ? path : resolve(path, 'index.ts');
        return {
          category: categoryDir,
          name: basename(dirOrFileItem, extname(dirOrFileItem)),
          file,
        };
      });
    })
    .reduce(
      (all, curr) => ({
        ...all,
        [`lambdas/${curr.category}/${curr.name}`]: curr.file,
      }),
      {}
    );
};
//   return readdirSync(srcDir)
//     .map(categoryDir => {
//       return readdirSync(resolve(srcDir, categoryDir)).map(dir => {
//         return readdirSync(resolve(srcDir, categoryDir, dir))
//           .filter(dirOrFile => {
//             const from = resolve(srcDir, categoryDir, dir, dirOrFile);
//             const indexFilePath = `${dirname(posixPath(from))}/index.ts`;
//             return from.includes('index.ts') || !existsSync(indexFilePath);
//           })
//           .map(dirOrFile => {
//             const name = `-${dirOrFile.replace(/\.[^.]*$/, '')}`.replace('-index', '');

//             if (categoryDir == 'apis' && !name) {
//               return readdirSync(resolve(srcDir, categoryDir, dir))
//                 .filter(dirOrFile2 => dirOrFile2 != 'index.ts')
//                 .map(dirOrFile2 => {
//                   const name = `/${dirOrFile2.replace(/\.[^.]*$/, '')}`.replace('/index', '');
//                   return {
//                     category: categoryDir,
//                     name: `${singular(dir)}${name}`,
//                     file:
//                       dirOrFile2 == 'index.ts'
//                         ? resolve(srcDir, '../lib/apiIndex.ts')
//                         : resolve(srcDir, categoryDir, dir, dirOrFile2),
//                   };
//                 });
//             }

//             return [
//               {
//                 category: categoryDir,
//                 name: `${singular(dir)}${name}`,
//                 file: resolve(srcDir, categoryDir, dir, dirOrFile),
//               },
//             ];
//           });
//       });
//     })
//     .reduce((all, curr) => all.concat(curr))
//     .reduce((all, curr) => all.concat(curr))
//     .reduce((all, curr) => all.concat(curr))
//     .reduce(
//       (all, curr) => ({
//         ...all,
//         [`lambdas/${curr.category}/${curr.name}`]: curr.file,
//       }),
//       {}
//     );
// };

// const getLambdaFiles = (from: any) => {
//   from = posixPath(from);

//   if (!from.includes('/index.ts')) {
//     return [resolve(from)];
//   }

//   return readdirSync(dirname(from))
//     .filter(file => {
//       return file.match(/.*\.ts$/);
//     })
//     .map(file => resolve(resolve(dirname(from), file)));
// };

// const setSchemaValidationParameters = (schema: TJS.Definition | null, params: string[]): TJS.Definition | null => {
//   if (schema && params) {
//     params.forEach((param: string) => {
//       if (schema.definitions && schema.definitions.hasOwnProperty(param)) {
//         schema.properties = {
//           ...(schema.properties || {}),
//           [param]: {
//             $ref: `#/definitions/${param}`,
//           },
//         };
//         schema.oneOf = [...(schema.oneOf || []), { required: [param] }];
//       }
//     });
//   }

//   return schema;
// };

// const getDescriptionFromPath = (from: string) => {
//   const { category, type, name }: any =
//     /^.*\/(?<category>[^\/.]+)\/(?<type>[^\/.]+)\/(?<name>[^\/.]+)\.ts$/.exec(from)?.groups || {};

//   console.log({ from, category, type, name });

//   if (category === 'lambdas') {
//     return {
//       category: type,
//       type: '',
//       name: singular(name),
//     };
//   }

//   return { category, type: singular(type), name };
// };

// const getSchemaPath = (from: string) => {
//   const source = posixPath(from).replace('/index.ts', '.ts');
//   const indexFilePath = `${dirname(posixPath(from))}/index.ts`;
//   const { category, type, name }: any = getDescriptionFromPath(source);

//   const separator = category === 'apis' && existsSync(indexFilePath) ? '/' : '-';
//   const cleanedName = `${separator}${name}`.replace(`${separator}index`, '');
//   const typeAndName = `${type}${cleanedName}`.replace(/^[-\/]*(.*?)[-\/]*$/g, '$1');
//   return `lambdas/${category}/${typeAndName}/schema.json`;
// };

// const jsonSchemaPlugin = new CopyWebpackPlugin({
//   patterns: [
//     {
//       from: resolve(lambdasSrcDir, '**/*.ts'),
//       transformPath(_targetPath: string, from: string) {
//         return getSchemaPath(from);
//       },
//       transform(_content: Buffer, from: string) {
//         const time = new Date();

//         const source = posixPath(from).replace('/index.ts', '.ts');
//         const { category, type, name }: any = getDescriptionFromPath(source);

//         const schemaRelative = getSchemaPath(from);
//         const schemaFile = source.replace(/(^.*)\/src\/.*$/, `$1/build/${schemaRelative}`);

//         if (existsSync(schemaFile) && statSync(schemaFile).mtimeMs > statSync(from).mtimeMs) {
//           console.log(`Skipping ${schemaRelative}`);
//           utimesSync(schemaFile, time, time);
//           return readFileSync(schemaFile, 'utf-8');
//         }

//         console.log(`Generating ${schemaRelative}`);

//         const schemaID = [category, type, name].join('-');
//         const program = TJS.getProgramFromFiles(getLambdaFiles(from));
//         let schema = TJS.generateSchema(
//           program,
//           '*',
//           {
//             excludePrivate: true,
//             ignoreErrors: true,
//             required: true,
//             id: schemaID,
//           },
//           getLambdaFiles(from)
//         );

//         schema = setSchemaValidationParameters(schema, validationParameters);

//         console.log(`${schemaRelative} generated.`);

//         if (existsSync(schemaFile)) {
//           utimesSync(schemaFile, time, time);
//         }

//         return JSON.stringify(schema);
//       },
//     },
//     // {
//     //   from: resolve(lambdasSrcDir, 'apis/**/*.ts'),
//     //   filter(from: string) {
//     //     return from.includes('index.ts');
//     //   },
//     //   transformPath(_targetPath: string, from: string) {
//     //     return getSchemaPath(from).replace('schema.json', 'index.js');
//     //   },
//     //   transform(_content: Buffer, _from: string) {
//     //     return ts.transpile(readFileSync(resolve(srcDir, 'lib/apiIndex.ts'), 'utf-8'));
//     //   },
//     // },
//   ],
// });
