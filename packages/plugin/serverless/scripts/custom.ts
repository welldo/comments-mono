import { GraphQLObjectType, GraphQLSchema } from 'graphql';

export function plugin(schema: GraphQLSchema) {
  //, _documents, _config, _info
  const typesMap = schema.getTypeMap();

  const roots = ['Query', 'Mutation', 'Subscription'];

  const lambdas = roots.reduce((agg, root) => {
    const t = typesMap[root] as GraphQLObjectType;
    const fields = Object.keys(t.getFields()).reduce((all, f) => {
      const directiveNodes = t.getFields()?.[f]?.astNode?.directives;
      const directives = directiveNodes && directiveNodes.length ? directiveNodes.map(d => d.name.value) : undefined;
      return all.concat({ name: f, directives } as any);
    }, []);
    return agg.concat({ root, fields } as any);
  }, []);

  return JSON.stringify(lambdas, null, 2);
}
