import { sign, verify, Algorithm } from 'jsonwebtoken';
import yargs = require('yargs/yargs');

// configure cli params
const argv = yargs(process.argv.slice(2)).options({
  user: { type: 'string', demandOption: true },
  secret: { type: 'string', demandOption: false, default: '6DAF318621F33431CFDD829E48378' }, // fixed token
  issuer: { type: 'string', demandOption: false, default: 'powtoon.com' },
  expiresIn: { type: 'number', demandOption: false, default: 60 * 60 * 60 }, // set for 60 days
  alg: { type: 'string', demandOption: false, default: 'HS512' }, // make sure it is one of the Algorithm types
}).argv;

const token = sign({ userId: argv.user }, argv.secret, {
  issuer: argv.issuer,
  algorithm: argv.alg as Algorithm,
  expiresIn: argv.expiresIn,
});

console.log(token);
console.log(verify(token, argv.secret));
