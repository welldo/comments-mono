import AWS, { DynamoDB } from 'aws-sdk';
import { DynamoDBRecord } from 'aws-lambda';
import axios from 'axios';
import { isEqual } from 'lodash';
export interface MutationType {
  query: string;
  operationName: string;
  variables: {
    changeInfo: string;
    powtoonId: string;
  };
}

function getOperationName(_record: DynamoDBRecord) {
  return 'notifyRecordChange';
}

function getUnmarshalledData(record: DynamoDBRecord) {
  const dynamoImage = record.eventName === 'REMOVE' ? record.dynamodb!.Keys! : record.dynamodb!.NewImage!;
  return DynamoDB.Converter.unmarshall(dynamoImage);
}

function getChangeInfo(unmarshalledData: { [key: string]: any }, eventName: string, eventSourceARN: string) {
  const changeInfo = {
    event: eventName,
    eventSourceARN,
    record: unmarshalledData,
  };

  return changeInfo;
}

function createMutation(operationName: string, powtoonId: string, changeInfo: any): MutationType {
  const mutationGql = `mutation ${operationName}($powtoonId: String!, $changeInfo: AWSJSON!) {
    ${operationName}(powtoonId: $powtoonId, changeInfo: $changeInfo ) {
      powtoonId
      changeInfo
    }
  }`;

  console.log(operationName, powtoonId, changeInfo);

  return {
    query: mutationGql,
    operationName,
    variables: {
      powtoonId,
      changeInfo: JSON.stringify(changeInfo),
    },
  };
}

export function changeAllowMutation(record: DynamoDBRecord) {
  if (record.eventName === 'REMOVE' || record.eventName === 'INSERT') return true;
  const dynamoNewImage = DynamoDB.Converter.unmarshall(record.dynamodb!.NewImage!);
  const dynamoOldImage = DynamoDB.Converter.unmarshall(record.dynamodb!.OldImage!);

  if (dynamoNewImage.readBy == undefined && dynamoOldImage.readBy == undefined) {
    return true; // unread last person
  }

  if (dynamoNewImage.readBy == undefined && dynamoOldImage.readBy != undefined) {
    return false; // unread last person
  }

  if (dynamoNewImage.readBy != undefined && dynamoOldImage.readBy == undefined) {
    return false; // read first person
  }
  // if values are equal, then no changes happened
  return isEqual(dynamoNewImage.readBy.values.sort(), dynamoOldImage.readBy.values.sort());
}

export function createMutationFromRecord(record: DynamoDBRecord) {
  const unmarshalledData = getUnmarshalledData(record);
  const operationName = getOperationName(record);
  const powtoonId = unmarshalledData.powtoonId;
  const changeInfo = getChangeInfo(unmarshalledData, record.eventName!, record.eventSourceARN!);
  const mutation = createMutation(operationName, powtoonId, changeInfo);
  return mutation;
}

export const executeMutation = async (mutation: MutationType) => {
  try {
    const appsyncEndpoint = process.env.APPSYNC_ENDPOINT;
    const endpoint = new AWS.Endpoint(appsyncEndpoint as string);
    const request = new AWS.HttpRequest(endpoint, process.env.APPSYNC_API_REGION as string);

    request.method = 'POST';
    request.body = JSON.stringify(mutation);
    request.headers['host'] = (appsyncEndpoint as string).split('https://')[1].split('/')[0];
    request.headers['Content-Type'] = 'application/json';

    const creds = AWS.config.credentials; //new AWS.EnvironmentCredentials('AWS');
    const signer = new (AWS as any).Signers.V4(request, 'appsync', true);
    signer.addAuthorization(creds, new Date());

    const response = await axios({
      method: 'POST',
      url: appsyncEndpoint,
      data: JSON.parse(request.body),
      headers: request.headers,
    });

    console.log('RESPONSE', JSON.stringify(response.data));
    return JSON.stringify(response.data);
  } catch (error) {
    console.error(`[ERROR] ${JSON.stringify(error, null, 4)}`);
    throw error;
  }
};
