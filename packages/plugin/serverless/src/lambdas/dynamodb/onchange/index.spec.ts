import { DynamoDBRecord, DynamoDBStreamEvent } from 'aws-lambda/trigger/dynamodb-stream';

import { handler } from './';

import { getDummyRecord, data as testDatas, getMockMutation } from './testHelper';
import * as utils from './utils';

process.env.TABLE = 'Comments';

const { testRegion, testPowtoonId, testId, testARN } = testDatas;
const mockMutation = getMockMutation('REMOVE', testARN, testPowtoonId, testId);

describe('event config', () => {
  let mockCreateMutationFromRecord: jest.SpyInstance<utils.MutationType>;
  let mockExecuteMutation: jest.SpyInstance<Promise<string>>;

  beforeEach(() => {
    mockCreateMutationFromRecord = jest.spyOn(utils, 'createMutationFromRecord').mockImplementation(() => mockMutation);
    mockExecuteMutation = jest.spyOn(utils, 'executeMutation').mockImplementation(async () => 'result');
  });

  afterEach(() => {
    mockCreateMutationFromRecord.mockReset();
    mockExecuteMutation.mockReset();
  });

  it.each([
    ['INSERT', JSON.stringify({ testRichText: 'test richtext', testSlideMillisec: '0' }), ''],
    [
      'MODIFY',
      JSON.stringify({ testRichText: 'new test richtext', testSlideMillisec: '1000', readBy: 'testUserId' }),
      JSON.stringify({ testRichText: 'new test richtext', testSlideMillisec: '1000', readBy: 'testUserId' }),
    ],
    ['REMOVE', '', ''],
  ])(
    "should create a(n) '%s' notifyRecordChange",
    async (testEventName: string, newImageOptions: string, oldImageOptions: string) => {
      // arrange
      let testRichText, oldTestRichText;
      let testSlideMillisec, oldTestSlideMillisec;
      let imageOptions;
      let originalImageOptions;
      let readBy;
      let oldReadBy;
      if (newImageOptions) {
        imageOptions = JSON.parse(newImageOptions);
        testRichText = imageOptions.testRichText;
        testSlideMillisec = imageOptions.testSlideMillisec;
        readBy = imageOptions.readBy;
      }

      if (oldImageOptions) {
        originalImageOptions = JSON.parse(oldImageOptions);
        oldTestRichText = originalImageOptions.testRichText;
        oldTestSlideMillisec = originalImageOptions.testSlideMillisec;
        oldReadBy = originalImageOptions.readBy;
      }

      const record = getDummyRecord(
        testEventName,
        {
          isImageOption: !!newImageOptions,
          testSlideMillisec,
          testRichText,
          readBy,
        },
        {
          isImageOption: !!oldImageOptions,
          testSlideMillisec: oldTestSlideMillisec,
          testRichText: oldTestRichText,
          readBy: oldReadBy,
        }
      );
      const event = {
        Records: [record],
      };

      // act
      const resp = await handler(event as DynamoDBStreamEvent);

      // if readby changed (no on change call)

      expect(resp).not.toBeFalsy();
      expect(mockCreateMutationFromRecord.mock.calls.length).toBe(1);
      const { eventName, awsRegion, dynamodb } = mockCreateMutationFromRecord.mock.calls[0][0] as DynamoDBRecord;
      expect(eventName).toBe(testEventName);
      expect(awsRegion).toBe(testRegion);

      if (dynamodb) {
        const { Keys, NewImage } = dynamodb;
        expect(Keys?.powtoonId.S).toBe(testPowtoonId);
        expect(Keys?.id.S).toBe(testId);

        if (imageOptions) {
          expect(NewImage?.slideMillisec.N).toBe(imageOptions?.testSlideMillisec);
          expect(NewImage?.richText.S).toBe(imageOptions?.testRichText);
        }
      }

      expect(mockExecuteMutation.mock.calls.length).toBe(1);
    }
  );

  it.each([
    [
      'MODIFY',
      JSON.stringify({ testRichText: 'new test richtext', testSlideMillisec: '1000', readBy: 'testUserId' }),
      '',
    ],
    [
      'MODIFY',
      '',
      JSON.stringify({ testRichText: 'new test richtext', testSlideMillisec: '1000', readBy: 'testUserId' }),
    ],
    [
      'MODIFY',
      JSON.stringify({ testRichText: 'new test richtext', testSlideMillisec: '1000', readBy: 'testUserId1' }),
      JSON.stringify({ testRichText: 'new test richtext', testSlideMillisec: '1000', readBy: 'testUserId2' }),
    ],
  ])(
    "shouldn't create a(n) '%s' notifyRecordChange",
    async (testEventName: string, newImageOptions: string, oldImageOptions: string) => {
      // arrange
      let testRichText, oldTestRichText;
      let testSlideMillisec, oldTestSlideMillisec;
      let imageOptions;
      let originalImageOptions;
      let readBy;
      let oldReadBy;
      if (newImageOptions) {
        imageOptions = JSON.parse(newImageOptions);
        testRichText = imageOptions.testRichText;
        testSlideMillisec = imageOptions.testSlideMillisec;
        readBy = imageOptions.readBy;
      }

      if (oldImageOptions) {
        originalImageOptions = JSON.parse(oldImageOptions);
        oldTestRichText = originalImageOptions.testRichText;
        oldTestSlideMillisec = originalImageOptions.testSlideMillisec;
        oldReadBy = originalImageOptions.readBy;
      }

      const record = getDummyRecord(
        testEventName,
        {
          isImageOption: !!newImageOptions,
          testSlideMillisec,
          testRichText,
          readBy,
        },
        {
          isImageOption: !!oldImageOptions,
          testSlideMillisec: oldTestSlideMillisec,
          testRichText: oldTestRichText,
          readBy: oldReadBy,
        }
      );
      const event = {
        Records: [record],
      };

      // act
      const resp = await handler(event as DynamoDBStreamEvent);

      expect(resp).toBeFalsy();
    }
  );

  it.each([
    [{}, undefined],
    [{ Records: [] }, null],
  ])(
    'should return undefined or null depending whether we give records or an empty records array',
    async (event: any, result: any) => {
      // arrange
      // act
      const resp = await handler(event);

      // assert
      expect(resp).toBe(result);
    }
  );

  it('for a correct record we should get answer', async () => {
    // arrange
    const testEventName = 'REMOVE';
    const record = getDummyRecord(testEventName);
    const event = {
      Records: [record],
    };

    // act
    const resp = await handler(event as DynamoDBStreamEvent);

    // assert
    expect(resp).not.toBeFalsy();
    expect(mockCreateMutationFromRecord.mock.calls.length).toBe(1);
    const { eventName, awsRegion, dynamodb } = mockCreateMutationFromRecord.mock.calls[0][0] as DynamoDBRecord;
    expect(eventName).toBe(testEventName);
    expect(awsRegion).toBe(testRegion);
    if (dynamodb) {
      const { Keys } = dynamodb;
      expect(Keys?.powtoonId.S).toBe(testPowtoonId);
      expect(Keys?.id.S).toBe(testId);
    }
    expect(mockExecuteMutation.mock.calls.length).toBe(1);
  });
});
