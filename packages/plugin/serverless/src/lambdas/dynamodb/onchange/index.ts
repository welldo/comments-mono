import { DynamoDBStreamEvent } from 'aws-lambda';

import { createMutationFromRecord, executeMutation, changeAllowMutation } from './utils';

export const handler = async (event: DynamoDBStreamEvent) => {
  if (event.Records) {
    for (const record of event.Records) {
      // console.log(JSON.stringify(process.env, null, 2));
      // console.log(JSON.stringify(record, null, 2));

      if (!changeAllowMutation(record)) {
        return null;
      }

      const mutation = createMutationFromRecord(record);
      return await executeMutation(mutation);
    }

    return null;
  }
};
