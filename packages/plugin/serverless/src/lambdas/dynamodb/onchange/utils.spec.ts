import { DynamoDBRecord } from 'aws-lambda';
import axios from 'axios';
import * as AWSMock from 'jest-aws-sdk-mock';
import AWS from 'aws-sdk';

import { executeMutation, createMutationFromRecord } from './utils';
import { getDummySimpleRecord, data as testData, getMockMutation } from './testHelper';

const { testARN, testPowtoonId, testId } = testData;

jest.mock('axios', () => jest.fn());

const mockMutation = getMockMutation('REMOVE', testARN, testPowtoonId, testId);

describe('utils', () => {
  beforeEach(() => {
    AWSMock.restore();
    ((axios as unknown) as jest.Mock).mockReset();
  });

  it.each([
    ['INSERT', JSON.stringify({ testRichText: 'test rich text', testSlideMillisec: '0' })],
    ['REMOVE', ''],
  ])('test createMutation function', (testEventName: string, newImageOptions: string) => {
    // arrange
    let testRichText;
    let testSlideMillisec;
    let imageOptions;
    if (newImageOptions) {
      imageOptions = JSON.parse(newImageOptions);
      testRichText = imageOptions.testRichText;
      testSlideMillisec = imageOptions.testSlideMillisec;
    }
    const record = getDummySimpleRecord(testEventName, {
      isImageOption: !!newImageOptions,
      testSlideMillisec,
      testRichText,
    });

    // act
    const mutation = createMutationFromRecord(record as DynamoDBRecord);

    // assert
    expect(mutation.query).toBeDefined();
    expect(mutation.operationName).toBe('notifyRecordChange');
    expect(mutation.variables.changeInfo).toBeDefined();
    expect(mutation.variables.powtoonId).toBe(mockMutation.variables.powtoonId);
  });

  it('test executeMutation function should fail', async () => {
    // arrange
    process.env.APPSYNC_ENDPOINT = 'https://nwkdl3sz5nenbibpgnv6rddxnm.appsync-api.eu-west-1.amazonaws.com/graphql';
    process.env.APPSYNC_API_REGION = 'eu-west-1';

    ((axios as unknown) as jest.Mock).mockRejectedValueOnce(new Error('my network error'));

    try {
      // act
      await executeMutation(mockMutation);
      // assert
      expect(1).toBe(2);
    } catch (err) {
      expect(err).not.toBeFalsy();
    }
  });

  it('test executeMutation function should pass', async () => {
    // arrange
    process.env.APPSYNC_ENDPOINT = 'https://nwkdl3sz5nenbibpgnv6rddxnm.appsync-api.eu-west-1.amazonaws.com/graphql';
    process.env.APPSYNC_API_REGION = 'eu-west-1';

    ((axios as unknown) as jest.Mock).mockResolvedValueOnce({ data: 'success' });
    AWSMock.setSDKInstance(AWS);
    AWSMock.mock('Signers.V4', 'addAuthorization', () => jest.fn());

    const resp = await executeMutation(mockMutation);

    // assert
    expect(resp).toBe('"success"');
  });
});
