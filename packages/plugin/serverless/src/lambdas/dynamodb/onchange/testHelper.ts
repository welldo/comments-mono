const testARN = 'arn:aws:lambda:eu-west-1:086104865349:function:appsync-resolvers';
const testRegion = 'eu-west-1';
const testPowtoonId = 'cfa211ff-28a1-4295-ace7-6fda192a28e5';
const testId = 'cfa211ff-28a1-4295-ace7-6fda192a28e5@test#2021-03-27T11:45:16.609Z';
const testSlideId = 'f7fce69f-610d-4aa1-9bd4-11cdec068cfb';
const streamViewType = 'NEW_IMAGE';
const testUserId = 'userId';

export const data = {
  testARN,
  testRegion,
  testPowtoonId,
  testId,
  testSlideId,
  streamViewType,
  testUserId,
};

export const getDummyRecord = (
  testEventName: string,
  newImageOptions?: { isImageOption: boolean; testSlideMillisec: number; testRichText: string; readBy: string },
  oldImageOptions?: { isImageOption: boolean; testSlideMillisec: number; testRichText: string; readBy: string }
) => ({
  eventID: 'ec366dea8e3380a74f13df2348f46a39',
  eventName: testEventName,
  eventVersion: '1.1',
  eventSource: 'aws:dynamodb',
  awsRegion: testRegion,
  dynamodb: {
    ApproximateCreationDateTime: 1616845516,
    Keys: {
      powtoonId: {
        S: testPowtoonId,
      },
      id: {
        S: testId,
      },
    },
    SequenceNumber: '19080300000000004671558913',
    SizeBytes: 371,
    StreamViewType: streamViewType,
    ...(newImageOptions?.isImageOption && {
      NewImage: {
        createdAt: {
          S: new Date().toISOString(),
        },
        slideId: {
          S: testSlideId,
        },
        powtoonId: {
          S: testPowtoonId,
        },
        slideMillisec: {
          N: newImageOptions.testSlideMillisec,
        },
        id: {
          S: testId,
        },
        richText: {
          S: newImageOptions.testRichText,
        },
        userId: {
          S: testUserId,
        },
        updatedAt: {
          S: new Date().toISOString(),
        },
        readBy: {
          SS: [newImageOptions.readBy || 'testUserId'],
        },
      },
    }),
    ...(oldImageOptions?.isImageOption && {
      OldImage: {
        createdAt: {
          S: new Date().toISOString(),
        },
        slideId: {
          S: testSlideId,
        },
        powtoonId: {
          S: testPowtoonId,
        },
        slideMillisec: {
          N: oldImageOptions.testSlideMillisec,
        },
        id: {
          S: testId,
        },
        richText: {
          S: oldImageOptions.testRichText,
        },
        userId: {
          S: testUserId,
        },
        updatedAt: {
          S: new Date().toISOString(),
        },
        readBy: {
          SS: [oldImageOptions.readBy || 'testUserId'],
        },
      },
    }),
  },
  eventSourceARN: testARN,
});

export const getDummySimpleRecord = (
  testEventName: string,
  newImageOptions?: { isImageOption: boolean; testSlideMillisec: number; testRichText: string }
) => ({
  eventID: 'ec366dea8e3380a74f13df2348f46a39',
  eventName: testEventName,
  eventVersion: '1.1',
  eventSource: 'aws:dynamodb',
  awsRegion: testRegion,
  dynamodb: {
    ApproximateCreationDateTime: 1616845516,
    Keys: {
      powtoonId: {
        S: testPowtoonId,
      },
      id: {
        S: testId,
      },
    },
    SequenceNumber: '19080300000000004671558913',
    SizeBytes: 371,
    StreamViewType: streamViewType,
    ...(newImageOptions?.isImageOption && {
      NewImage: {
        createdAt: {
          S: new Date().toISOString(),
        },
        slideId: {
          S: testSlideId,
        },
        powtoonId: {
          S: testPowtoonId,
        },
        slideMillisec: {
          N: newImageOptions.testSlideMillisec,
        },
        id: {
          S: testId,
        },
        richText: {
          S: newImageOptions.testRichText,
        },
        userId: {
          S: testUserId,
        },
        updatedAt: {
          S: new Date().toISOString(),
        },
      },
    }),
  },
  eventSourceARN: testARN,
});

export const getMockMutation = (eventName: string, arn: string, powtoonId: string, id: string) => ({
  query:
    'mutation notifyRecordChange( \n' +
    '    $changeInfo: AWSJSON!, $powtoonId: String!\n' +
    '  ) {\n' +
    '    notifyRecordChange(\n' +
    '      powtoonId: $powtoonId\n' +
    '      changeInfo: $changeInfo\n' +
    '    )\n' +
    '     {\n' +
    '      powtoonId\n' +
    '      changeInfo\n' +
    '    }\n' +
    '  }',
  operationName: 'notifyRecordChange',
  variables: {
    changeInfo: `{"event":"${eventName}","eventSourceARN":"${arn}","record":{"powtoonId":"${powtoonId}","id":"${id}"}}`,
    powtoonId: powtoonId,
  },
});
