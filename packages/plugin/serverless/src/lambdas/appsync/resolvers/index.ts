import { AppSyncResolverHandler } from 'aws-lambda';
import { verify, Algorithm, VerifyOptions } from 'jsonwebtoken';

import { getSecretByName } from '~/lib/secret';

import * as resolvers from './resolvers';

const AWS_ALGO = (process.env.JWT_ALG as Algorithm) || 'HS512';

type AppSyncHandler = AppSyncResolverHandler<any, any>;

/**
 *
 * @param token Verify JWT and return data object if exists.
 * @param secret
 * @param options
 * @returns Object
 */
function verifyPayload(token: string, secret: string, options: VerifyOptions): any {
  return verify(token, secret, options);
}

/**
 * find the authenticated user (Server-to-Server or Client-to-Server)
 * @param event
 * @returns string | null
 */
const getUserId = async (event: Parameters<AppSyncHandler>[0]): Promise<string | any> => {
  // @ts-ignore
  let userId = event?.identity?.username;
  const jwtSecretName = process.env.JWT_SECRET_NAME;
  const jwtConfig = await getSecretByName(jwtSecretName!);
  const jwtPublicKey = jwtConfig.JWT_PUBLIC_KEY;

  console.log(JSON.stringify({ message: 'appsync::resolvers::index::getUserId() dispatched', data: userId }, null, 2));
  if (!userId) {
    const authorization = event.request.headers['Authorization'] || event.request.headers['authorization'];
    const token = authorization?.replace('Bearer ', '');

    if (token) {
      try {
        const payload = verifyPayload(token, jwtPublicKey, {
          algorithms: [AWS_ALGO],
        });
        userId = payload!.userId;
      } catch (err) {
        //@TODO: if payload doesn't compile to user id, shall we halt?
      }
    }
    console.log(JSON.stringify({ message: 'appsync::resolvers::index::getUserId() token', data: token }, null, 2));
  }
  return userId;
};

export const handler: AppSyncHandler = async event => {
  console.log(JSON.stringify({ event }, null, 2));
  const functionName = event.info.fieldName as keyof typeof resolvers;
  const fn = resolvers[functionName];
  let userId = await getUserId(event);

  if (functionName === 'notifyRecordChange') {
    userId = 'set aws_iam user';
  }

  if (!fn || !userId) {
    throw new Error('Missing info in event');
  }
  return fn(userId, event.arguments);
};
