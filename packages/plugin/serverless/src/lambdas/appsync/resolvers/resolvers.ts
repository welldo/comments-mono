import { execute, executeSql, executeBatch, createRecord, updateRichTextField } from '~/lib/dynamodb';

import {
  Comment,
  MutationAddCommentArgs,
  MutationAddReplyArgs,
  MutationNotifyRecordChangeArgs,
  MutationRemoveCommentArgs,
  MutationRemoveReplyArgs,
  MutationResolveCommentArgs,
  MutationUpdateCommentsTimeArgs,
  MutationUpdateCommentTextArgs,
  MutationUpdateReplyTextArgs,
  MutationUpdateCommentReadStatusArgs,
  QueryCommentsArgs,
  SubscriptionOnRecordChangedArgs,
} from './generated/schema.types';
import { CommentTableItem, encodeCommentId, encodeReplyId, getPowtoonId, ReplyTableItem } from './utils';

const tableName = process.env.TABLE_NAME || 'Comments';

export async function comments(userId: string, { powtoonId }: QueryCommentsArgs): Promise<Array<Comment>> {
  const items = await executeSql(`SELECT * FROM "${tableName}" WHERE powtoonId='${powtoonId}'`);
  return items
    .map(c => {
      c.isRead = c.readBy && c.readBy.values && c.readBy.values.indexOf(userId) !== -1 ? true : false;
      return c;
    })
    .map(c => {
      if (c.id.split('#').length === 2) c.replies = items.filter(r => r.id.includes(c.id) && c.id != r.id);
      return c;
    })
    .filter((c: Comment) => c.id.split('#').length == 2);
}

export async function addComment(
  userId: string,
  { powtoonId, richText, slideId, slideMillisec }: MutationAddCommentArgs
): Promise<string> {
  const createdAt = new Date().toISOString();
  const id = encodeCommentId({ userId, powtoonId, createdAt });
  const item: CommentTableItem = {
    id,
    createdAt,
    updatedAt: createdAt,
    powtoonId,
    richText,
    slideId,
    slideMillisec,
    userId,
  };
  await createRecord(tableName, item);

  return id;
}

export async function addReply(userId: string, { replyToId, richText }: MutationAddReplyArgs): Promise<string> {
  const createdAt = new Date().toISOString();
  const id = encodeReplyId({ userId, replyToId, createdAt });
  const powtoonId = getPowtoonId(replyToId);
  const item: ReplyTableItem = {
    id,
    powtoonId,
    createdAt,
    updatedAt: createdAt,
    richText,
    userId,
    replyToId,
  };
  await createRecord(tableName, item);

  return id;
}

export async function updateCommentText(
  userId: string,
  { id, richText }: MutationUpdateCommentTextArgs
): Promise<boolean> {
  const powtoonId = getPowtoonId(id);
  await updateRichTextField(tableName, { id, powtoonId, richText });
  return true;
}

export async function resolveComment(userId: string, { id, status }: MutationResolveCommentArgs): Promise<boolean> {
  const powtoonId = getPowtoonId(id);
  const resolvedAtValue = status ? `'${new Date().toISOString()}'` : 'NULL';
  await execute(
    `UPDATE "${tableName}" SET resolvedAt=${resolvedAtValue} WHERE id='${id}' AND powtoonId='${powtoonId}'`
  );
  return true;
}

export async function removeComment(userId: string, { id }: MutationRemoveCommentArgs): Promise<boolean> {
  const powtoonId = getPowtoonId(id);
  await execute(`DELETE FROM "${tableName}" WHERE id='${id}' AND powtoonId='${powtoonId}'`);
  return true;
}

export async function updateReplyText(userId: string, { id, richText }: MutationUpdateReplyTextArgs): Promise<boolean> {
  const powtoonId = getPowtoonId(id);
  await updateRichTextField(tableName, { id, powtoonId, richText });
  return true;
}

export async function removeReply(userId: string, { id }: MutationRemoveReplyArgs): Promise<boolean> {
  const powtoonId = getPowtoonId(id);
  await execute(`DELETE FROM "${tableName}" WHERE id='${id}' AND powtoonId='${powtoonId}'`);
  return true;
}

export async function updateCommentsTime(
  userId: string,
  { powtoonId, slideId, startMillisec, incrementMillisec }: MutationUpdateCommentsTimeArgs
): Promise<number> {
  const items = await executeSql(
    `SELECT * FROM "${tableName}" WHERE powtoonId='${powtoonId}' AND slideId='${slideId}' AND slideMillisec>=${startMillisec}`
  );

  if (!items || !items.length) return 0;

  const statements = items.map(comment => ({
    Statement: `UPDATE "${tableName}" 
    SET slideMillisec=${Number(comment.slideMillisec) + Number(incrementMillisec)} 
    SET updatedAt='${new Date().toISOString()}'
    WHERE id='${comment.id}' AND powtoonId='${getPowtoonId(comment.id)}'`,
  }));

  await executeBatch({ Statements: statements });
  return items.length;
}

export async function updateCommentReadStatus(
  userId: string,
  { id, status }: MutationUpdateCommentReadStatusArgs
): Promise<boolean> {
  const powtoonId = getPowtoonId(id);

  if (status === true) {
    await execute(
      `UPDATE "${tableName}" SET readBy=SET_ADD(readBy,<<'${userId}'>>) WHERE id='${id}' AND powtoonId='${powtoonId}'`
    );
  } else {
    await execute(
      `UPDATE "${tableName}" SET readBy=SET_DELETE(readBy,<<'${userId}'>>) WHERE id='${id}' AND powtoonId='${powtoonId}'`
    );
  }
  return true;
}

function isUserAllowedWatching(userId: string, powtoonId: string) {
  // TODO: something like a role based authorization implementation needed
  const allowed = true;
  console.log({ userId, powtoonId, allowed });
  return allowed;
}

export async function onRecordChanged(userId: string, args: SubscriptionOnRecordChangedArgs & { changeInfo: any }) {
  if (!isUserAllowedWatching(userId, args.powtoonId)) {
    throw new Error('Cannot listen to changed');
  }

  return {
    powtoonId: args.powtoonId,
    changeInfo: args.changeInfo,
  };
}

export async function notifyRecordChange(_userId: string, args: MutationNotifyRecordChangeArgs) {
  // user id is from iam!
  return args;
}
