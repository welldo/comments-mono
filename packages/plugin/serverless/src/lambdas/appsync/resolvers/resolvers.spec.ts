import { AWSError, DynamoDB } from 'aws-sdk';
import { BatchExecuteStatementOutput } from 'aws-sdk/clients/dynamodb';
import { PromiseResult } from 'aws-sdk/lib/request';

import * as dynamodbExecuters from '~/lib/dynamodb';

import { MutationNotifyRecordChangeArgs } from './generated/schema.types';
import {
  addComment,
  addReply,
  comments,
  notifyRecordChange,
  onRecordChanged,
  removeComment,
  removeReply,
  resolveComment,
  updateCommentReadStatus,
  updateCommentsTime,
  updateCommentText,
  updateReplyText,
} from './resolvers';

process.env.TABLE = 'dev-CommentsStack-devdinamotable86CDEE21-17P8LAI2M4VJY';

const testPowtoonId = 'powtoonId';
const testUserId = 'userId';
const testSlideId = 'slideId';
const dummyComment = {
  powtoonId: testPowtoonId,
  richText: 'richText',
  slideMillisec: 0,
  slideId: testSlideId,
};
const mockComent = {
  id: 'cfa211ff-28a1-4295-ace7-6fda192a28e5@testUser#2021-04-20T07:17:08.648Z',
  richText: 'c1',
  createdAt: '2021-04-20T07:17:08.648Z',
  powtoonId: 'cfa211ff-28a1-4295-ace7-6fda192a28e5',
  slideId: '1eff8bd8-39b7-435c-b551-01f3bb40538a',
  slideMillisec: 0,
  replies: [
    {
      id: 'cfa211ff-28a1-4295-ace7-6fda192a28e5@testUser#2021-04-20T07:17:08.648Z#testUser#2021-04-20T07:17:21.807Z',
      richText: 'c1 r1',
      createdAt: '',
      replies: [],
    },
  ],
  position: 1,
};

describe('resolverTests', () => {
  let mockExecutSql: jest.SpyInstance<Promise<any[]>>;
  let mockExecute: jest.SpyInstance<Promise<PromiseResult<DynamoDB.ExecuteStatementOutput, AWSError>>>;

  beforeEach(() => {
    mockExecutSql = jest.spyOn(dynamodbExecuters, 'executeSql').mockImplementation(async () => []);
    mockExecute = jest.spyOn(dynamodbExecuters, 'execute').mockImplementation(
      async () =>
        ({
          Items: [],
        } as any)
    );
  });

  afterEach(() => {
    mockExecutSql.mockReset();
    mockExecute.mockReset();
  });

  describe('comments', () => {
    it.each([
      [[]],
      [[mockComent]],
      [[{ ...mockComent, ...{ readBy: { values: ['anotherUserId'] } } }]],
      [[{ ...mockComent, ...{ readBy: { values: ['userId'] } } }]],
    ])('retrieving comments', async commentsToReturn => {
      // arrange
      mockExecutSql.mockImplementationOnce(async () => commentsToReturn);
      // act
      const commentRes = await comments(testUserId, { powtoonId: testPowtoonId });

      // assert
      expect(mockExecutSql.mock.calls.length).toBe(1);
      expect(mockExecutSql.mock.calls[0][0]).toMatch(/SELECT \* FROM \".+\" WHERE powtoonId=\'.+\'/);
      expect(commentRes.length).toBeGreaterThanOrEqual(0);
    });
  });

  describe('addComments', () => {
    it('adding a comment', async () => {
      // arrange
      // act
      const id = await addComment(testUserId, dummyComment);

      // assert
      expect(mockExecute.mock.calls.length).toBe(1);
      expect(mockExecute.mock.calls[0][0]).toMatch(/INSERT INTO \"[a-zA-Z]+\" VALUE \{((\'.+\'\:\'.+\')(|\,))+\}/);
      expect(id).not.toBeFalsy();
      expect(id).toMatch(/[a-zA-Z0-9]+@[a-zA-Z0-9]+#[0-9]{4}\-[0-9]{2}\-[0-9A-Z\.\:]+/);
    });
  });

  describe('updateCommentText', () => {
    it('updating a comment text', async () => {
      // arrange
      const newRichText = 'new comment text';
      const id = await addComment(testUserId, dummyComment);
      mockExecute.mockReset();

      // act
      const isSucceeded = await updateCommentText(testUserId, {
        richText: newRichText,
        id: id,
      });

      // assert
      expect(mockExecute.mock.calls.length).toBe(1);
      expect(mockExecute.mock.calls[0][0]).toMatch(
        /UPDATE \"[a-zA-Z\-0-9]+\" SET richText=\'[a-zA-Z0-9 ]*\' SET updatedAt=\'.+\' WHERE id=\'[a-zA-Z0-9]+@[a-zA-Z0-9]+#[0-9]{4}\-[0-9]{2}\-[0-9A-Z\.\:]+\' AND powtoonId=\'[a-zA-Z0-9]+\'/
      );
      expect(isSucceeded).toBe(true);
    });
  });

  describe('resolveComment', () => {
    it('resolve a comment', async () => {
      // arrange
      const id = await addComment(testUserId, dummyComment);
      mockExecute.mockReset();

      // act
      const isSucceeded = await resolveComment(testUserId, { id });

      // assert
      expect(mockExecute.mock.calls.length).toBe(1);
      expect(mockExecute.mock.calls[0][0]).toMatch(
        /UPDATE \"[a-zA-Z\-0-9]+\" SET resolvedAt=\'[0-9]{4}\-[0-9]{2}\-[0-9A-Z\.\:]+\' SET updatedAt=\'.+\' WHERE id=\'[a-zA-Z0-9]+@[a-zA-Z0-9]+#[0-9]{4}\-[0-9]{2}\-[0-9A-Z\.\:]+\' AND powtoonId=\'[a-zA-Z0-9]+\'/
      );
      expect(isSucceeded).toBe(true);
    });
  });

  describe('updateCommentsTime', () => {
    it.each([
      [0, [mockComent]],
      [30000, []],
    ])(
      "update comment's millisec if it's current value is greater or equal than %i",
      async (startValue, commentsToReturn) => {
        // arrange
        mockExecutSql.mockImplementationOnce(async () => commentsToReturn);
        await addComment(testUserId, dummyComment);
        mockExecute.mockReset();

        const mockExecuteBatch = jest
          .spyOn(dynamodbExecuters, 'executeBatch')
          .mockImplementation(
            async () =>
              ({ Responses: [{ TableName: 'Comments' }] } as PromiseResult<BatchExecuteStatementOutput, AWSError>)
          );

        // act
        await updateCommentsTime(testUserId, {
          incrementMillisec: 1000,
          powtoonId: testPowtoonId,
          slideId: testSlideId,
          startMillisec: startValue,
        });

        // assert
        expect(mockExecutSql.mock.calls.length).toBe(1);
        expect(mockExecutSql.mock.calls[0][0]).toBe(
          `SELECT * FROM "Comments" WHERE powtoonId='${testPowtoonId}' AND slideId='${testSlideId}' AND slideMillisec>=${startValue}`
        );
        expect(mockExecuteBatch.mock.calls.length).toBe(1);
      }
    );
  });

  describe('updateCommentReadStatus', () => {
    it.each([true, false])(
      "update comment's millisec if it's current value is greater or equal than %i",
      async readStatus => {
        // arrange
        const newCommentsId = await addComment(testUserId, dummyComment);
        mockExecute.mockReset();

        // act
        const isSucceeded = await updateCommentReadStatus(testUserId, {
          id: newCommentsId,
          status: readStatus,
        });

        // assert
        expect(isSucceeded).not.toBeFalsy();
        expect(mockExecute.mock.calls.length).toBe(1);
        if (readStatus === true) {
          expect(mockExecute.mock.calls[0][0]).toBe(
            `UPDATE "Comments" SET readBy=SET_ADD(readBy,<<'${testUserId}'>>) WHERE id='${newCommentsId}' AND powtoonId='${testPowtoonId}'`
          );
        }

        if (readStatus === false) {
          expect(mockExecute.mock.calls[0][0]).toBe(
            `UPDATE "Comments" SET readBy=SET_DELETE(readBy,<<'${testUserId}'>>) WHERE id='${newCommentsId}' AND powtoonId='${testPowtoonId}'`
          );
        }
      }
    );
  });

  describe('addReply', () => {
    it('adding a reply', async () => {
      // arrange
      // act
      const newCommentsId = await addComment(testUserId, dummyComment);
      mockExecute.mockReset();
      const id = await addReply(testUserId, { replyToId: newCommentsId, richText: 'reply richText' });

      // assert
      expect(id).not.toBeFalsy();
      expect(mockExecute.mock.calls.length).toBe(1);
      expect(mockExecute.mock.calls[0][0]).toMatch(/INSERT INTO "Comments" VALUE \{.*'richText':'reply richText'.*\}/);
    });
  });

  describe('updateReplyText', () => {
    it('updating a reply text', async () => {
      // arrange
      const newRichText = 'reply new richText';
      const newCommentsId = await addComment(testUserId, dummyComment);
      mockExecute.mockReset();
      const id = await addReply(testUserId, { replyToId: newCommentsId, richText: 'reply2 richText' });
      mockExecute.mockReset();

      // act
      const isSucceeded = await updateReplyText(testUserId, {
        richText: newRichText,
        id: id,
      });

      // assert
      expect(isSucceeded).toBe(true);
      expect(mockExecute.mock.calls.length).toBe(1);
      expect(mockExecute.mock.calls[0][0]).toMatch(/UPDATE "Comments" SET richText='reply new richText' .*/);
    });
  });

  describe('removeReply', () => {
    it('remove a reply', async () => {
      // arrange
      const newCommentsId = await addComment(testUserId, dummyComment);
      mockExecute.mockReset();
      const createdId = await addReply(testUserId, { replyToId: newCommentsId, richText: 'reply to remove' });
      mockExecute.mockReset();

      // act
      const isSucceeded = await removeReply(testUserId, {
        id: createdId,
      });

      // assert
      expect(isSucceeded).not.toBeFalsy();
      expect(mockExecute.mock.calls.length).toBe(1);
      expect(mockExecute.mock.calls[0][0]).toBe(
        `DELETE FROM "Comments" WHERE id='${createdId}' AND powtoonId='${testPowtoonId}'`
      );
    });
  });

  describe('removeComment', () => {
    it('remove a comment', async () => {
      // arrange
      const createdId = await addComment(testUserId, dummyComment);
      mockExecute.mockReset();

      // act
      const isSucceeded = await removeComment(testUserId, {
        id: createdId,
      });

      // assert
      expect(isSucceeded).toBe(true);
      expect(mockExecute.mock.calls.length).toBe(1);
      expect(mockExecute.mock.calls[0][0]).toBe(
        `DELETE FROM "Comments" WHERE id='${createdId}' AND powtoonId='${testPowtoonId}'`
      );
    });
  });

  describe('onRecordChange', () => {
    it.each([
      [testUserId],
      // TODO: userId for the unauthorized user subscription test case
    ])('subscription to a powtoon', async (userId: string) => {
      // arrange
      const recordChangeArgs = {
        powtoonId: '45f1c361-a194-413d-a85a-62c3c4734ec5',
        changeInfo: {
          event: 'MODIFY',
          eventSourceARN: 'arn:aws:dynamodb:eu-west-2:379112755632:table/Comments/stream/2021-04-08T04:49:48.068',
          record: {
            createdAt: '2021-04-09T04:08:00.992Z',
            slideId: '9a4c8d65-2130-44cf-ab44-04e2258d746a',
            powtoonId: '45f1c361-a194-413d-a85a-62c3c4734ec5',
            slideMillisec: 1000,
            id: '45f1c361-a194-413d-a85a-62c3c4734ec5@testUser#2021-04-09T04:08:00.992Z',
            richText: 'add comment',
            userId: 'testUser',
            updatedAt: '2021-04-09T04:10:23.795Z',
          },
        },
      };

      // act
      try {
        const resp = await onRecordChanged(userId, recordChangeArgs);

        // assert
        expect(resp).toMatchObject(recordChangeArgs);
      } catch (err) {
        expect(err.message).toBe('Cannot listen to changed');
      }
    });
  });

  describe('notifyRecordChange', () => {
    it('testing notifyRecordChange', async () => {
      // arrange
      const testArguments = { powtoonId: 'powtoonId' };

      // act
      const respArgs = await notifyRecordChange(testUserId, testArguments as MutationNotifyRecordChangeArgs);

      // assert
      expect(respArgs.powtoonId).toBe(testArguments.powtoonId);
    });
  });
});
