import { Comment, Maybe, Scalars } from './generated/schema.types';

/**
 * See https://abba.dev/blog/dynamodb-partiql-javascript
 *
 * Table structure
 *
 * powtoonId    - (PartitionKey) UserId (SortKey)
 * id           - unique for each row
 * createdAt
 * updatedAt
 * removedAt
 * richText
 *  --
 *  slideId
 *  slideMillisec
 *  thumbnail
 *  resolvedAt
 *  ---
 *  replyToId
 */

export interface CommentTableItem extends Omit<Comment, '__typename' | 'replies'> {
  removedAt?: Maybe<Scalars['String']>;
}

export interface ReplyTableItem extends Omit<CommentTableItem, 'slideId' | 'slideMillisec' | 'resolvedAt'> {
  replyToId?: Maybe<Scalars['String']>;
}

export type CommentId = Pick<CommentTableItem, 'powtoonId' | 'userId' | 'createdAt'>;
export type ReplyId = Pick<ReplyTableItem, 'userId' | 'createdAt' | 'replyToId'>;
export type RichTextFieldItem = Pick<Comment, 'id' | 'powtoonId' | 'richText'>;
// export type ReplyIdEx = ReplyId & Pick<ReplyTableItem, 'powtoonId'>;

export function encodeCommentId({ powtoonId, userId, createdAt }: CommentId): string {
  return `${powtoonId}@${userId}#${createdAt}`;
}
export function encodeReplyId({ replyToId, userId, createdAt }: ReplyId): string {
  return `${replyToId}#${userId}#${createdAt}`;
}
export function getPowtoonId(id: string) {
  return id.split('@')[0];
}

// export function decodeId(replyId: string): ReplyIdEx {
//   const {
//     powtoonId,
//     replyToId,
//     userId,
//     createdAt,
//   }: any = /^(?<replyToId>(?<powtoonId>[^@]+).*)(@|#)(?<userId>[^#]+)#(?<createdAt>[^#]+)$/g.exec(replyId)?.groups;
//   return { replyToId, userId, createdAt, powtoonId };
// }
