import { handler } from './';

import * as resolvers from './resolvers';
import { Comment, MutationNotifyRecordChangeArgs } from './generated/schema.types';

process.env.TABLE = 'Comments';
// token with 10 years expiration
const token =
  'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJ1c2VySWQiLCJpYXQiOjE2MTg4MDgxMDgsImV4cCI6MTYzMTk0ODEwOCwiaXNzIjoicG93dG9vbi5jb20ifQ.eNaBUAtjlk2GFbB_i3VPWXL3tfeqTPzZBkeOhtATWEPQo6EKza8NUaOB1YmYzMJXsDlTLyW4geCuAeV2jHDeeA';

const context = {
  callbackWaitsForEmptyEventLoop: false,
  functionName: '',
  functionVersion: '',
  invokedFunctionArn: '',
  memoryLimitInMB: '1024',
  awsRequestId: '1',
  logGroupName: '',
  logStreamName: '',
  getRemainingTimeInMillis: () => 100,
  done: () => undefined,
  fail: () => undefined,
  succeed: () => undefined,
};
const callback = () => undefined;
const getDummyEvent = (args: any, headers: { [key: string]: string }, fieldName: string) => ({
  identity: {
    sub: 'sub',
    issuer: '',
    claims: '',
    sourceIp: ['127.0.0.1'],
    defaultAuthStrategy: 'testStrategy',
  } as any,
  arguments: args,
  request: {
    headers,
  },
  info: {
    selectionSetList: [''],
    selectionSetGraphQL: '',
    parentTypeName: '',
    fieldName,
    variables: {},
  },
  source: null,
  prev: null,
  stash: {},
});

const getDummyDynamodbEvent = (args: any, headers: { [key: string]: string }, fieldName: string) => ({
  identity: {
    sub: 'sub',
    issuer: '',
    claims: '',
    sourceIp: ['127.0.0.1'],
    defaultAuthStrategy: 'testStrategy',
    username: 'IAM_USER',
  } as any,
  arguments: args,
  request: {
    headers,
  },
  info: {
    selectionSetList: [''],
    selectionSetGraphQL: '',
    parentTypeName: '',
    fieldName,
    variables: {},
  },
  source: null,
  prev: null,
  stash: {},
});

describe('event config a', () => {
  let mockCommentsResolver: jest.SpyInstance<Promise<Array<Comment>>>;
  const testResolverName = 'comments';

  beforeEach(() => {
    mockCommentsResolver = jest.spyOn(resolvers, testResolverName).mockImplementation(
      async (): Promise<Array<Comment>> => {
        return [];
      }
    );
  });

  afterEach(() => {
    mockCommentsResolver.mockRestore();
  });

  it.each([
    ['userId', '', ''],
    ['', token, 'Authorization'],
    ['', token, 'authorization'],
    ['', '', 'Authorization'],
    ['', '', 'authorization'],
    ['', '', ''],
  ])('testing authorization', async (username: string, token: string, headerKey: string) => {
    // arrange
    const testPowtoonId = 'powtoonId';
    const headers: { [key: string]: string } = {};
    headers[headerKey] = `Bearer ${token}`;

    const event = getDummyEvent({ powtoonId: testPowtoonId }, headers, testResolverName);
    if (username) {
      event.identity.username = 'userId';
    } else {
      delete event.identity;
    }

    // act
    try {
      const resp = await handler(event, context, callback);
      expect(mockCommentsResolver.mock.calls.length).toBe(1);
      expect(mockCommentsResolver.mock.calls[0][0]).toBe('userId');
      expect(mockCommentsResolver.mock.calls[0][1].powtoonId).toBe(testPowtoonId);

      // assert
      expect(resp.length).toBeGreaterThanOrEqual(0);
    } catch (err) {
      expect(err.message).toBe('Missing info in event');
    }
  });

  it('testing notifyRecordChange "endpoint"', async () => {
    // arrange
    const testArguments = { powtoonId: 'powtoonId' };
    const event = getDummyDynamodbEvent(testArguments, {}, 'notifyRecordChange');
    const mockNotifyRecordChangeResolver = jest
      .spyOn(resolvers, 'notifyRecordChange')
      .mockImplementation(async () => testArguments as MutationNotifyRecordChangeArgs);

    // act
    try {
      const resp = await handler(event, context, callback);
      expect(mockNotifyRecordChangeResolver.mock.calls.length).toBe(1);
      expect(mockNotifyRecordChangeResolver.mock.calls[0][1].powtoonId).toBe(testArguments.powtoonId);

      // assert
      const { powtoonId } = resp;
      expect(powtoonId).toBe(testArguments.powtoonId);
    } catch (err) {
      expect(err.message).toBe('Missing info in event');
    }

    mockNotifyRecordChangeResolver.mockClear();
  });
});
