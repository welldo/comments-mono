/* eslint-disable */
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  AWSJSON: any;
};


export type Comment = {
  readonly __typename?: 'Comment';
  readonly id: Scalars['ID'];
  readonly createdAt: Scalars['String'];
  readonly updatedAt: Scalars['String'];
  readonly userId: Scalars['String'];
  readonly isRead?: Maybe<Scalars['Boolean']>;
  readonly powtoonId: Scalars['String'];
  readonly richText: Scalars['String'];
  readonly slideId: Scalars['String'];
  readonly slideMillisec: Scalars['Int'];
  readonly resolvedAt?: Maybe<Scalars['String']>;
  readonly replies?: Maybe<ReadonlyArray<Maybe<Reply>>>;
};

export type Mutation = {
  readonly __typename?: 'Mutation';
  readonly addComment: Scalars['ID'];
  readonly addReply: Scalars['ID'];
  readonly updateCommentText?: Maybe<Scalars['Boolean']>;
  readonly resolveComment?: Maybe<Scalars['Boolean']>;
  readonly removeComment?: Maybe<Scalars['Boolean']>;
  readonly updateCommentsTime: Scalars['Int'];
  readonly updateReplyText?: Maybe<Scalars['Boolean']>;
  readonly removeReply?: Maybe<Scalars['Boolean']>;
  readonly updateCommentReadStatus?: Maybe<Scalars['Boolean']>;
  readonly notifyRecordChange?: Maybe<RecordChangeEvent>;
};


export type MutationAddCommentArgs = {
  powtoonId: Scalars['String'];
  richText: Scalars['String'];
  slideId: Scalars['String'];
  slideMillisec: Scalars['Int'];
};


export type MutationAddReplyArgs = {
  replyToId: Scalars['ID'];
  richText: Scalars['String'];
};


export type MutationUpdateCommentTextArgs = {
  id: Scalars['ID'];
  richText: Scalars['String'];
};


export type MutationResolveCommentArgs = {
  id: Scalars['ID'];
  status?: Maybe<Scalars['Boolean']>;
};


export type MutationRemoveCommentArgs = {
  id: Scalars['String'];
};


export type MutationUpdateCommentsTimeArgs = {
  powtoonId: Scalars['String'];
  slideId: Scalars['String'];
  startMillisec: Scalars['Int'];
  incrementMillisec: Scalars['Int'];
};


export type MutationUpdateReplyTextArgs = {
  id: Scalars['ID'];
  richText: Scalars['String'];
};


export type MutationRemoveReplyArgs = {
  id: Scalars['String'];
};


export type MutationUpdateCommentReadStatusArgs = {
  id: Scalars['ID'];
  status?: Maybe<Scalars['Boolean']>;
};


export type MutationNotifyRecordChangeArgs = {
  powtoonId: Scalars['String'];
  changeInfo: Scalars['AWSJSON'];
};

export type Query = {
  readonly __typename?: 'Query';
  readonly comments: ReadonlyArray<Maybe<Comment>>;
};


export type QueryCommentsArgs = {
  powtoonId: Scalars['String'];
};

export type RecordChangeEvent = {
  readonly __typename?: 'RecordChangeEvent';
  readonly powtoonId: Scalars['String'];
  readonly changeInfo?: Maybe<Scalars['AWSJSON']>;
};

export type Reply = {
  readonly __typename?: 'Reply';
  readonly id: Scalars['ID'];
  readonly replyToId?: Maybe<Scalars['ID']>;
  readonly createdAt: Scalars['String'];
  readonly updatedAt: Scalars['String'];
  readonly userId: Scalars['String'];
  readonly richText: Scalars['String'];
  readonly isRead: Scalars['Boolean'];
};

export type Subscription = {
  readonly __typename?: 'Subscription';
  readonly onRecordChanged?: Maybe<RecordChangeEvent>;
};


export type SubscriptionOnRecordChangedArgs = {
  powtoonId: Scalars['String'];
};
