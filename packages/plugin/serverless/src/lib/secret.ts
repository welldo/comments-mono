import { SecretsManager } from 'aws-sdk';

export interface SecretConfig {
  JWT_PUBLIC_KEY: string;
}

export async function getSecretByName(secretName: string): Promise<SecretConfig> {
  const secretsManager = new SecretsManager();
  const { SecretString } = await secretsManager
    .getSecretValue({
      SecretId: secretName,
    })
    .promise();

  const secret = JSON.parse(SecretString!) as SecretConfig;

  return secret;
}
