import { DynamoDB } from 'aws-sdk';
import { BatchExecuteStatementInput, Converter, UpdateItemInput } from 'aws-sdk/clients/dynamodb';

import { RichTextFieldItem } from '../lambdas/appsync/resolvers/utils';

export async function execute(stmt: string) {
  const dynamodb = new DynamoDB();
  return await dynamodb
    .executeStatement({
      Statement: stmt,
    })
    .promise();
}

// TODO: consider execute in transaction e.g. `transactWriteItems`
export async function executeBatch(stmt: BatchExecuteStatementInput) {
  const dynamodb = new DynamoDB();
  return await dynamodb.batchExecuteStatement(stmt).promise();
}

export async function executeSql<TResultItem = any>(sqlString: string): Promise<TResultItem[]> {
  const { Items } = await execute(sqlString);

  return Items?.map((i: Record<string, any>) => Converter.unmarshall(i) as TResultItem) || [];
}

export async function createRecord(tableName: string, record: { [key: string]: any }) {
  const dynamodb = new DynamoDB();
  const formedItems = Converter.marshall(record);
  const params = {
    TableName: tableName,
    Item: formedItems,
  };

  return await dynamodb.putItem(params).promise();
}

export async function updateRichTextField(tableName: string, item: RichTextFieldItem) {
  const dynamodb = new DynamoDB();
  const params = {
    TableName: tableName,
    Key: {
      id: { S: item.id },
      powtoonId: { S: item.powtoonId },
    },
    UpdateExpression: 'set updatedAt = :x, richText = :y',
    ExpressionAttributeValues: {
      ':x': { S: new Date().toISOString() },
      ':y': { S: item.richText },
    },
  } as UpdateItemInput;

  return await dynamodb.updateItem(params).promise();
}
