import { BatchExecuteStatementInput, Converter } from 'aws-sdk/clients/dynamodb';

import { execute, executeBatch, executeSql } from './dynamodb';

const mockExecuteStatement = jest.fn().mockImplementation((stmt: any) => ({
  promise: async () => Promise.resolve({ Items: [{ stmt: stmt.Statement }] }),
}));

const mockBatchExecuteStatement = jest.fn().mockImplementation(({ Statements: stmts }) => ({
  promise: async () => Promise.resolve(stmts),
}));

const mockUnmarshall = jest.fn().mockImplementation((data: { [key: string]: any }): { [key: string]: any } => data);

jest.mock('aws-sdk', () => ({
  DynamoDB: function () {
    return {
      executeStatement: mockExecuteStatement,
      batchExecuteStatement: mockBatchExecuteStatement,
    };
  },
}));

jest.mock('aws-sdk/clients/dynamodb');

describe('dynamodb tests', () => {
  afterEach(() => {
    mockExecuteStatement.mockClear();
    mockBatchExecuteStatement.mockClear();
  });

  it('testing the execute function', async () => {
    const stmt = 'stmt';
    const { Items } = await execute(stmt);
    expect(Items![0].stmt).toBe(stmt);
    expect(mockExecuteStatement.mock.calls.length).toBe(1);
  });

  it('testing the executeBatch function', async () => {
    const stmts = { Statements: [{ Statement: 'stmt' }] };
    const resp = await executeBatch(stmts as BatchExecuteStatementInput);
    expect(resp).toBe(stmts.Statements);
    expect(mockBatchExecuteStatement.mock.calls.length).toBe(1);
  });

  it.each([[undefined], [[{ stmt: 'stmt' }]]])(
    'testing the executeSql function',
    async (items: { [key: string]: any }[] | undefined) => {
      const stmt = 'stmt';
      mockExecuteStatement.mockImplementation((_: any) => ({
        promise: async () => Promise.resolve({ Items: items }),
      }));
      Converter.unmarshall = mockUnmarshall;
      const resp = await executeSql(stmt);
      console.log('resp: ', resp);
      expect(resp).toMatchObject(items || []);
      expect(mockExecuteStatement.mock.calls.length).toBe(1);
    }
  );
});
