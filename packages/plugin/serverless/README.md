
- https://docs.aws.amazon.com/appsync/latest/devguide/security-authorization-use-cases.html
- https://aws.amazon.com/blogs/mobile/appsync-custom-auth/
- https://docs.aws.amazon.com/appsync/latest/devguide/aws-appsync-real-time-data.html
- https://appsync-immersionday.workshop.aws/
- https://hackernoon.com/graphql-authorization-with-multiple-data-sources-using-aws-appsync-dfae2e350bf2
- https://serverless.pub/the-power-of-serverless-graphql-with-appsync/
- https://aws.amazon.com/blogs/mobile/appsync-realtime/
- https://dev.to/danielbayerlein/aws-appsync-without-authentication-3fnm (iam + idp for anonymous)