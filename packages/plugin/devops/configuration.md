# Configurations

## Setup build environment
###### Install nvm(nodejs version manager)
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
    source ~/.bashrc

###### Install cypress linux dependencies 
    visit this for the command https://docs.cypress.io/guides/continuous-integration/introduction#Dependencies

###### Install node
    nvm install v14.4.0

######  Install yarn 
    npm install --global yarn

######  Install cdk
    npm install --global cdk

######  To avoid modules incompatibility errors
    yarn config set ignore-engines true

######  Go to commenting-system repo and run cdk bootstrap command, to bootstrap stack
    cdk bootstrap aws://AWS_ACCOUNT_ID/AWS_REGION

# Deploy commenting-system
######  Install dependencies
    yarn --network-timeout 600

######  Deploy with one command to specific env using --context argument
     yarn _plugin-devops deploy --context stage=<Env Prefix VALUE> --context baseDomainName=<domain:example.com> --context hostedZoneId=<domain hosted Zone ID> --context asmPublicKeyName=<AWS Secret Manager Name> --context jwtAlgorithm=<JWT Key Algo> --context appSyncApiKeyExpiration=<Days of Validity> --profile {$profile if not default}

> ## Set the region env variable
 - modify the **.aws/credentials** file:
 ```
[default]
...
region=<your_aws_region>
```
> ## packages/plugin/client
 - build it
```bash
  cd packages/plugin/client
  yarn build
```
---
> ## packages/plugin/serverless
 - build it
```bash
  cd packages/plugin/serverless
  yarn build
```
---
> ## packages/plugin/devops
 - then synth & deploy
```bash
  cd packages/plugin/devops
  yarn synth
  yarn deploy
```
---
> ## Deploy with one command
- go to the root and run:
```bash
yarn deploy-plugin
```
---
> ## packages/plugin/client
 - make sure that the proper values are in the **packages/plugin/client/src/shared/aws.config.ts** file
 - you can get these data (endpoint, region and api_key) from the terminal after a deploy or from the aws console at AppSync Settings
```typescript
import { AUTH_TYPE } from 'aws-appsync-auth-link';
import { createAuthLink } from 'aws-appsync-auth-link';

export const awsConfig: Parameters<typeof createAuthLink>[0] = {
  url:
    process.env.REACT_APP_AWS_ENDPOINT ||
    <your_appsync_endpoint_as_a_string>,
  region: process.env.REACT_APP_AWS_REGION || <your_region_as_a_string>,
  auth: {
    type: AUTH_TYPE.API_KEY,
    apiKey: process.env.REACT_APP_AWS_APPSYNC_KEY || <your_api_key_as_a_string>,
  },
};
```
---
> > If everything is set up and you still get a 401:
```bash
  cd packages/plugin/devops
  yarn destroy
  yarn synth
  yarn deploy
```
