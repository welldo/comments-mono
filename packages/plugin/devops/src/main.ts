import 'source-map-support/register';
import { App, Tags } from '@aws-cdk/core';

import CommentsStack from './CommentsStack';
import { setStage } from './lib/utils';

const app = new App();
setStage(app, app.node.tryGetContext('stage') || 'dev');
Tags.of(app).add('Department', 'RnD');
Tags.of(app).add('Owner', 'DevOps');
Tags.of(app).add('Created By', `CDK - commenting system in ${app.stageName} environment`);

new CommentsStack(app);

if (process.env.TS_NODE_DEV === 'true') {
  console.dir(app.synth({}).artifacts);
}
