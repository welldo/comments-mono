import {
  CloudFrontWebDistribution,
  OriginProtocolPolicy,
  SecurityPolicyProtocol,
  SSLMethod,
} from '@aws-cdk/aws-cloudfront';
import { Bucket } from '@aws-cdk/aws-s3';
import { BucketDeployment, Source } from '@aws-cdk/aws-s3-deployment';
import { RemovalPolicy, Tags } from '@aws-cdk/core';
import { CloudFrontTarget } from '@aws-cdk/aws-route53-targets';
import { Construct } from '@aws-cdk/core';
import { ARecord, HostedZone, RecordTarget, HostedZoneAttributes, IAliasRecordTarget } from '@aws-cdk/aws-route53';
import { DnsValidatedCertificate } from '@aws-cdk/aws-certificatemanager';

import { artifactId, stageIs, getStageName } from '../lib/utils';

const createARecord = (
  stack: Construct,
  name: string,
  targetAlias: IAliasRecordTarget,
  zoneAttributes: HostedZoneAttributes
) => {
  try {
    return new ARecord(stack, `${name}-arecord`, {
      zone: HostedZone.fromHostedZoneAttributes(stack, artifactId(`${name}-hostedzone`, stack), zoneAttributes),
      target: RecordTarget.fromAlias(targetAlias),
    });
  } catch (err) {
    console.log(err);
  }
};
const buildSubdomain = (stack: Construct, config: any, subdomainName: string) => {
  return `${subdomainName}${!stageIs(stack, 'prod') ? `-${getStageName(stack)}` : ''}.${config.baseDomainName}`;
};

const createCertificate = (
  stack: Construct,
  name: string,
  props: {
    domain: string;
    hostedZoneId: string;
    region?: string;
  }
) => {
  return new DnsValidatedCertificate(stack, `${name}-s3certificate`, {
    domainName: props.domain,
    hostedZone: HostedZone.fromHostedZoneAttributes(stack, `${name}-s3hostedZone`, {
      zoneName: props.domain,
      hostedZoneId: props.hostedZoneId,
    }),
    region: props.region,
  });
};

export interface StaticSiteProps {
  baseDomainName: string;
  siteSubDomain: string;
  assetsPath: string;
  hostedZoneId: string;
}

export default class StaticSite extends Construct {
  public readonly siteUrl: string;

  constructor(parent: Construct, id: string, props: StaticSiteProps) {
    super(parent, id);

    const siteDomain = buildSubdomain(this, props, props.siteSubDomain);

    this.siteUrl = `https://${siteDomain}`;

    const siteBucket = new Bucket(this, `${id}-bucket`, {
      bucketName: `${getStageName(this)}-s3-bucket-comments`,
      websiteIndexDocument: 'index.html',
      websiteErrorDocument: 'error.html',
      removalPolicy: RemovalPolicy.DESTROY, // TODO: ensure ok for production code
      publicReadAccess: true,
      autoDeleteObjects: true,
    });

    Tags.of(siteBucket).add('Creation Date', 'TBD');
    Tags.of(siteBucket).add('Purpose', 'S3 bucket for commenting FE');

    const certificateArn = createCertificate(this, `${id}`, {
      domain: siteDomain,
      hostedZoneId: props.hostedZoneId,
      region: 'us-east-1',
    }).certificateArn;

    const httpsDistribution = new CloudFrontWebDistribution(this, `${id}-s3distribution-comments`, {
      aliasConfiguration: {
        acmCertRef: certificateArn,
        names: [siteDomain],
        sslMethod: SSLMethod.SNI,
        securityPolicy: SecurityPolicyProtocol.TLS_V1_1_2016,
      },
      originConfigs: [
        {
          customOriginSource: {
            domainName: siteBucket.bucketWebsiteDomainName,
            originProtocolPolicy: OriginProtocolPolicy.HTTP_ONLY,
          },
          behaviors: [{ isDefaultBehavior: true }],
        },
      ],
      errorConfigurations: [
        {
          errorCode: 403,
          responsePagePath: '/index.html',
          responseCode: 200,
        },
        {
          errorCode: 404,
          responsePagePath: '/index.html',
          responseCode: 200,
        },
      ],
    });

    Tags.of(httpsDistribution).add('Purpose', 'CDN for commenting system');

    createARecord(this, `${id}-site`, new CloudFrontTarget(httpsDistribution), {
      hostedZoneId: props.hostedZoneId,
      zoneName: siteDomain,
    });

    new BucketDeployment(this, `${id}-s3deployment-comments`, {
      sources: [Source.asset(props.assetsPath)],
      destinationBucket: siteBucket,
      distribution: httpsDistribution,
      distributionPaths: ['/*'],
      retainOnDelete: false,
    });
  }
}
