import { App, Tags, Construct, ConstructNode, CfnResource } from '@aws-cdk/core';
import { camelCase, kebabCase, upperFirst } from 'lodash';

export const setStage = (app: App, stage: string) => {
  (app as any).stageName = stage; // workaround readonly
  Tags.of(app).add('stage', app.stageName);
};

export const getStageName = (stack: Construct) => {
  return App.of(stack)?.stageName;
};

export const artifactId = (name: string, parent?: Construct & { artifactId?: string }) => {
  return `${parent?.artifactId || ''}${parent?.artifactId ? '-' : ''}${kebabCase(name)}`;
};

export const resourceId = (node: ConstructNode, newName: string) => {
  const logicalName = upperFirst(camelCase(newName));
  (node.defaultChild! as CfnResource).overrideLogicalId(logicalName);
};

export const stageIs = (stack: Construct, stage: string) => {
  return getStageName(stack) === stage;
};

export const stagedValue = (stack: Construct, name: string) => {
  return !stageIs(stack, 'prod') ? `${getStageName(stack)}${upperFirst(name)}` : name;
};
