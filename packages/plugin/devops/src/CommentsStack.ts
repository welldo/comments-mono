/* eslint-disable @typescript-eslint/no-unused-vars */
import { resolve } from 'path';
import { readFileSync } from 'fs';

import {
  App,
  Stack,
  RemovalPolicy,
  CfnOutput,
  CfnResource,
  Duration,
  Expiration,
  Tags,
  IAspect,
  IConstruct,
  Aspects,
} from '@aws-cdk/core';
import { Function as LambdaFunction, Runtime, Code, StartingPosition } from '@aws-cdk/aws-lambda';
import { AuthorizationType, GraphqlApi, Schema } from '@aws-cdk/aws-appsync';
import { AttributeType, BillingMode, ProjectionType, StreamViewType, Table } from '@aws-cdk/aws-dynamodb';
import { DynamoEventSource } from '@aws-cdk/aws-lambda-event-sources';
import { Policy, Role, PolicyStatement } from '@aws-cdk/aws-iam';
import { Algorithm } from 'jsonwebtoken';

import StaticSite from './constructs/StaticSite';
import { artifactId, getStageName, resourceId } from './lib/utils';

type ResolverInfo = {
  root: 'Subscription' | 'Mutation' | 'Query';
  fields: [
    {
      name: string;
      directives: ['aws_subscribe' | 'aws_api_key' | 'aws_iam'];
    }
  ];
};

type PowtoonPublicKeyProps = {
  secretsManagerName: string;
  alg: string;
};

class ResourceNameUpdater implements IAspect {
  public visit(node: IConstruct): void {
    const path = node.toString().split('/');
    if (node instanceof Role) {
      resourceId(node.node, `Powtoon-${path[path.length - 2]}Role`);
    }

    if (node instanceof Policy) {
      resourceId(node.node, `Powtoon-${path[path.length - 3]}Policy`);
    }

    if (node instanceof LambdaFunction) {
      if (node.toString().includes('CertificateRequestorFunction')) {
        resourceId(node.node, `${'lambda-s3-certificate'}`);
      }

      if (node.toString().includes('Custom::CDKBucketDeployment')) {
        resourceId(node.node, `lambda-s3-deployment`);
      }
      console.log('node', `${node.toString()}`, getStageName(node));
    }
  }
}

export default class CommentsStack extends Stack {
  constructor(scope: App) {
    super(scope, artifactId('Comments', scope));

    const lambdaAssetsDir =
      scope.node.tryGetContext('lambdaAssetsDir') || resolve(__dirname, '../../serverless/build/lambdas');

    const secretsManagerName = scope.node.tryGetContext('asmPublicKeyName');
    if (!secretsManagerName)
      throw new Error(
        'Missing Context Variable PUBLIC_KEY_NAME Value (cli: --context asmPublicKeyName=<PUBLIC_KEY_NAME>)'
      );
    // Read Authentication Secret / Algorithm to be passed to resolvers
    const jwtAlg = (scope.node.tryGetContext('jwtAlgorithm') as Algorithm) || 'HS512';
    const powtoonPublicKeyConfig: PowtoonPublicKeyProps = {
      secretsManagerName,
      alg: jwtAlg,
    };

    const commentsTable = this.createCommentsTable();

    const onChangeLambda = this.createDynamodbOnChangeLambda(lambdaAssetsDir, commentsTable, powtoonPublicKeyConfig);

    const { appSyncApi } = this.createAppSynchApi(lambdaAssetsDir, commentsTable, powtoonPublicKeyConfig);

    onChangeLambda.addEnvironment('APPSYNC_API_KEY', appSyncApi.apiKey || '');
    onChangeLambda.addEnvironment('APPSYNC_ENDPOINT', appSyncApi.graphqlUrl || '');
    onChangeLambda.addEnvironment('APPSYNC_API_REGION', this.region || '');

    appSyncApi.grantMutation(onChangeLambda);

    const site = new StaticSite(this, `${getStageName(this)}`, {
      baseDomainName: scope.node.tryGetContext('baseDomainName') || 'k8s.powtoon.com',
      siteSubDomain: 'powtoon-comments',
      assetsPath: '../client/dist',
      hostedZoneId: scope.node.tryGetContext('hostedZoneId') || 'Z01332863OT8AW8EX70TQ',
    });

    // parse all IAM Policies
    Aspects.of(this).add(new ResourceNameUpdater());

    new CfnOutput(this, 'site', { value: `${site.siteUrl}` });
    new CfnOutput(this, 'appsync-id', { value: appSyncApi.apiId });
    new CfnOutput(this, 'appsync-url', { value: appSyncApi.graphqlUrl });
    new CfnOutput(this, 'appsync-api-key', { value: appSyncApi.apiKey! });
    new CfnOutput(this, 'stack-region', { value: this.region });
  }

  private createAppSynchApi(
    lambdaAssetsDir: string,
    commentsTable: Table,
    powtoonPublicKeyConfig: PowtoonPublicKeyProps
  ) {
    const artifactId = `${getStageName(this)}-appsync-comments`;
    const expirationContext = this.node.tryGetContext('appSyncApiKeyExpiration');
    let expiration = Duration.days(365);
    if (expirationContext) {
      expiration = Duration.days(+expirationContext);
    }
    const appSyncApi = new GraphqlApi(this, artifactId, {
      name: `${getStageName(this)}-graphqlapi-comments`,
      schema: Schema.fromAsset(resolve(lambdaAssetsDir, 'appsync/resolvers/schema.gql')),
      authorizationConfig: {
        defaultAuthorization: {
          authorizationType: AuthorizationType.API_KEY,
          apiKeyConfig: {
            expires: Expiration.after(expiration),
          },
        },
        additionalAuthorizationModes: [
          {
            authorizationType: AuthorizationType.IAM,
          },
        ],
      },
      xrayEnabled: true,
    });

    Tags.of(appSyncApi).add('Purpose', 'GraphQL API for commenting system');
    resourceId(appSyncApi.node, artifactId);

    const resolversLambda = this.createLambda(
      `${getStageName(this)}-lambda-appsync-comments-resolvers`,
      resolve(lambdaAssetsDir, 'appsync/resolvers'),
      powtoonPublicKeyConfig
    );

    const resolversDatasource = appSyncApi.addLambdaDataSource(
      `${getStageName(this) ?? ''}_lambda_appsync_resolvers_dataSource_comments`,
      resolversLambda
    );

    const resolversInfo: ResolverInfo[] = JSON.parse(
      readFileSync(resolve(lambdaAssetsDir, 'appsync/resolvers/generated/schema.info.json'), 'utf-8')
    );

    resolversInfo.forEach(({ root: typeName, fields }) => {
      fields.forEach(({ name: fieldName }) => {
        resolversDatasource.createResolver({
          fieldName,
          typeName,
        });
      });
    });

    resolversLambda.addEnvironment('TABLE_NAME', commentsTable.tableName);
    resolversLambda.addEnvironment('JWT_ALG', powtoonPublicKeyConfig.alg);
    commentsTable.grantFullAccess(resolversLambda);

    return { appSyncApi, resolversLambda };
  }

  private createDynamodbOnChangeLambda(
    lambdaAssetsDir: any,
    commentsTable: Table,
    powtoonPublicKeyConfig: PowtoonPublicKeyProps
  ) {
    const lambdaName = `${getStageName(this)}-lambda-dynamodb-onchange-comments`;
    const onchangeLambda = this.createLambda(
      lambdaName,
      resolve(lambdaAssetsDir, 'dynamodb/onchange'),
      powtoonPublicKeyConfig
    );

    onchangeLambda.addEnvironment('TABLE_NAME', commentsTable.tableName);
    onchangeLambda.addEnvironment('JWT_SECRET_NAME', powtoonPublicKeyConfig.secretsManagerName);
    onchangeLambda.addEnvironment('JWT_ALG', powtoonPublicKeyConfig.alg);

    onchangeLambda.addEventSource(
      new DynamoEventSource(commentsTable, {
        startingPosition: StartingPosition.LATEST,
        batchSize: 5,
        bisectBatchOnError: true,
        retryAttempts: 10,
      })
    );

    Tags.of(onchangeLambda).add('Purpose', 'Lambda for listening to changes in commenting DB');

    return onchangeLambda;
  }

  private createCommentsTable() {
    const artifactId = `${getStageName(this)}-dynamodb-comments`;

    const table = new Table(this, artifactId, {
      tableName: `${getStageName(this)}Comments`,
      billingMode: BillingMode.PAY_PER_REQUEST,
      removalPolicy: RemovalPolicy.DESTROY,
      partitionKey: {
        name: 'powtoonId',
        type: AttributeType.STRING,
      },
      sortKey: {
        name: 'id',
        type: AttributeType.STRING,
      },
      stream: StreamViewType.NEW_AND_OLD_IMAGES,
    });

    table.addGlobalSecondaryIndex({
      indexName: 'userIdIndex',
      partitionKey: {
        name: 'userId',
        type: AttributeType.STRING,
      },
      projectionType: ProjectionType.KEYS_ONLY,
    });

    Tags.of(table).add('Purpose', 'DynamoDB table to hold commenting DB');
    resourceId(table.node, artifactId);

    return table;
  }

  private createLambda(name: string, codeDir: string, powtoonPublicKeyConfig: PowtoonPublicKeyProps) {
    const lambdaFunction = new LambdaFunction(this, name, {
      functionName: name,
      runtime: Runtime.NODEJS_12_X,
      handler: 'index.handler',
      code: Code.fromAsset(codeDir),
    });

    const statement = new PolicyStatement();
    statement.addResources('*');
    statement.addActions('secretsmanager:GetSecretValue');
    lambdaFunction.addToRolePolicy(statement);

    lambdaFunction.addEnvironment('JWT_SECRET_NAME', powtoonPublicKeyConfig.secretsManagerName);

    resourceId(lambdaFunction.node, name);

    return lambdaFunction;
  }
}
