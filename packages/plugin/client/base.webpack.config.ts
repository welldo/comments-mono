/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/explicit-function-return-type */

import path from 'path';

import { EnvironmentPlugin } from 'webpack';
import CopyPlugin from 'copy-webpack-plugin';
import ReactRefreshPlugin from '@pmmmwh/react-refresh-webpack-plugin';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import LoadablePlugin from '@loadable/webpack-plugin';
import dotenv from 'dotenv';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { loadableTransformer } from 'loadable-ts-transformer';
import tsImportPluginFactory from 'ts-import-plugin';
// import HardSourceWebpackPlugin from 'hard-source-webpack-plugin';

dotenv.config();

const isDevelopment = process.env.NODE_ENV !== 'production';

export default {
  devtool: 'source-map',
  output: {
    publicPath: process.env.PUBLIC_URL || '/',
  },
  mode: isDevelopment ? 'development' : 'production',
  devServer: {
    port: 3000,
    historyApiFallback: true,
  },
  entry: {
    main: './src/index.tsx',
  },
  module: {
    rules: [
      {
        test: /\.mjml$/,
        use: [
          {
            loader: 'webpack-mjml-loader',
            options: {
              minify: true,
              minifyOptions: {
                collapseWhitespace: true,
                minifyCSS: true,
                removeEmptyAttributes: true,
              },
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
          },
        ],
      },
      {
        test: /\.tsx?$/,
        // exclude: /node_modules/,
        use: [
          isDevelopment && {
            loader: 'babel-loader',
            options: { plugins: ['react-refresh/babel'] },
          },
          {
            loader: 'ts-loader',

            options: {
              transpileOnly: true,
              getCustomTransformers: () => ({
                before: [
                  loadableTransformer,
                  tsImportPluginFactory({
                    libraryDirectory: 'es',
                    libraryName: 'antd',
                    style: 'css',
                  }),
                ],
              }),
              compilerOptions: {
                module: 'es2015', // TODO: added due to issues with tsImportPluginFactory. still need to find implications
              },
            },
          },
        ].filter(Boolean),
      },
      {
        test: /\.css$/i,
        sideEffects: true,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif|webp)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
      {
        test: /\.ttf$/,
        use: ['file-loader'],
      },
    ],
  },
  plugins: [
    new EnvironmentPlugin(
      Object.keys(process.env).filter(key => key.startsWith('REACT_APP_') || ['NODE_ENV', 'DEBUG'].includes(key))
    ),
    new CopyPlugin({
      patterns: [
        {
          from: '**/*',
          context: path.resolve('./', 'public'),
          noErrorOnMissing: true,
        },
      ],
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      inject: true,
    }),
    new ForkTsCheckerWebpackPlugin(),
    new LoadablePlugin(),
    // new HardSourceWebpackPlugin(),
    isDevelopment && new ReactRefreshPlugin(),
  ].filter(Boolean),
  resolve: {
    alias: {
      '~': path.resolve('./', 'src/'),
    },
    extensions: ['.mjs', '.js', '.jsx', '.ts', '.tsx'],
    plugins: [new TsconfigPathsPlugin()],
  },
  optimization: {
    // usedExports: true,
    // minimize: false,
  },
};
