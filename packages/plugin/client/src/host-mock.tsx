import * as React from 'react';
import { render } from 'react-dom';
import { HostMock } from '@powtoon/host-lib';

render(<HostMock />, document.getElementById('app'));
