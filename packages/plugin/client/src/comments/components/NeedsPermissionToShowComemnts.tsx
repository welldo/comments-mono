import React from 'react';
import styled from 'styled-components';
import { Text, Button as _Button } from '@powtoon/design-system-components';

import givPlaceHolder from '~/shared/images/Gif-placeholder.png';

const MainWrapper = styled.div`
  justify-content: center;
  margin-top: -50%;
`;
const Image = styled.img`
  width: 175px;
  height: 175px;
  margin-bottom: 5px;
`;

const ShortTextWrappers = styled.div`
  display: flex;
  text-align: center;
  justify-content: center;
`;

const LongTextWrappers = styled.div`
  display: flex;
  text-align: center;
  margin-top: 7px;
  justify-content: center;
`;

const LongText = styled(Text)`
  letter-spacing: 0.14px !important;
`;

const ButtonWrapper = styled.div`
  padding-left: 43 px;
  padding-right: 43 px;
  padding-top: 18px;
`;

const NeedsPermissionToShowComments = ({}: // onInviteBtnClick,
//title,
//message,
{
  onInviteBtnClick: () => void;
  // title: string;
  message: string;
}) => {
  return (
    <MainWrapper>
      <Image src={givPlaceHolder} />
      <ShortTextWrappers>
        <Text fontStyle="h2">Your plan allows to comment-only.</Text>
      </ShortTextWrappers>
      <LongTextWrappers>
        <LongText fontStyle="small">
          Contact your admin to unlock your a full license to be able to edit, share and distribute.
        </LongText>
      </LongTextWrappers>
      <ButtonWrapper>
        <_Button iconName="crown" size="small" buttonType="premium" onClick={noref()}>
          Upgrade
        </_Button>
      </ButtonWrapper>
    </MainWrapper>
  );
};

export default NeedsPermissionToShowComments;
