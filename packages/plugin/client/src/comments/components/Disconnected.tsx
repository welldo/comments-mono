import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Text } from '@powtoon/design-system-components';

import { appConfig } from '~/shared/config/app.config';
import useHostStateData from '~/shared/hooks/useHostStateData';
import GifPlaceHolder from '~/shared/images/Gif-placeholder.png';

const Root = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
`;

const DisconnectedImg = styled.img`
  width: 175px;
  height: 175px;
  margin-bottom: 10px;
  filter: grayscale(100%);
`;

const DisconnectedText = styled(Text)`
  display: flex;
  text-align: center;
`;

const PING_RESOURCE = '/ping.txt';

const timeout = (time: number, promise: Promise<Response>) => {
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      reject(new Error('Request timed out.'));
    }, time);
    promise.then(resolve, reject);
  });
};

const checkOnlineStatus = async () => {
  const controller = new AbortController();
  const { signal } = controller;

  if (!navigator.onLine) return navigator.onLine;

  try {
    await timeout(
      appConfig.PingTimeout,
      fetch(PING_RESOURCE, {
        method: 'GET',
        signal,
      })
    );

    return true;
  } catch (error) {
    console.error(error);
    controller.abort();
  }

  return false;
};

const Disconnected = function ({ children }: { children: JSX.Element }) {
  const [hostDataState, hostDataActions] = useHostStateData();

  const checkStatus = async () => {
    const online = await checkOnlineStatus();
    hostDataActions.setOnlineStatus(online);
  };

  useEffect(() => {
    window.addEventListener('offline', () => {
      hostDataActions.setOnlineStatus(false);
    });

    // Add polling incase of slow connection
    const id = setInterval(() => {
      checkStatus();
    }, appConfig.OnlinePollingInterval);

    return () => {
      window.removeEventListener('offline', () => {
        hostDataActions.setOnlineStatus(false);
      });

      clearInterval(id);
    };
  });

  if (hostDataState.onlineStatus) return <>{children}</>;
  return (
    <Root>
      <DisconnectedImg src={GifPlaceHolder} />
      <DisconnectedText>
        Something went wrong. <br /> Try again later
      </DisconnectedText>
    </Root>
  );
};

export default Disconnected;
