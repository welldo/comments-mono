import React, { forwardRef, Ref } from 'react';
import styled from 'styled-components';
import {
  MoreOutlined,
  CheckCircleOutlined,
  DeleteOutlined,
  EditOutlined,
  EyeInvisibleOutlined,
} from '@ant-design/icons';

import { Comment } from '~/shared/recoil';
import Modal from '~/shared/components/Modal';
import Avatar from '~/shared/components/GuestAvatar';
import MoreButton from '~/shared/components/MoreButton';
import ModalButton from '~/shared/components/ModalButton';

const Root = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.divider};
  position: relative;
  padding: 10px;
  :hover {
    background: ${({ theme }) => theme.colors.backgroundHover};
  }
`;

const UserInfo = styled.div`
  display: flex;
  justify-content: flex-start;
  margin: 0 0 10px 0;
`;

const UserImage = styled.div`
  margin-right: 10px;
`;

const UserName = styled.div``;

const InfoLabel = styled.span<{ $isError?: boolean }>`
  color: ${({ theme, $isError }) => ($isError ? theme.colors.error : theme.colors.primary)};
`;

const formatNum = (num: number) => (num >= 10 ? `${num}` : `0${num}`);

const formatTime = (millisec?: number) => {
  if (!millisec) return '00:00';

  const seconds = Math.floor(millisec / 1000);

  if (seconds < 10) return `00:0${seconds}`;

  if (seconds < 60) return `00:${seconds}`;

  // if (seconds < 3600) {
  //   const minutes = Math.floor(seconds / 60);
  //   return `${formatNum(minutes)}:${formatNum(seconds - minutes * 60)}`;
  // }
  const minutes = Math.floor(seconds / 60);
  return `${formatNum(minutes)}:${formatNum(seconds - minutes * 60)}`;
  // const hours = Math.floor(seconds / 3600);

  // const minutes = Math.floor((seconds - hours * 3600) / 60);

  // return `${formatNum(hours)}:${formatNum(minutes)}:${formatNum(seconds - (hours * 3600 + minutes * 60))}`;
};

const CommentItem = (
  {
    comment,
    isMenuShown,
    onMenuChange,
    onDelete,
  }: {
    comment: Comment;
    isMenuShown: boolean;
    onMenuChange: () => void;
    onDelete: () => void;
  },
  ref: Ref<HTMLDivElement>
) => {
  const { id, createdAt, richText, slideId, slideMillisec } = comment;
  const isPositionUnknown = parseInt(slideId, 10) === -1;
  return (
    <Root key={id} ref={ref}>
      <MoreButton onClick={onMenuChange}>
        <MoreOutlined />
      </MoreButton>
      <Modal
        isShown={isMenuShown}
        hide={onMenuChange}
        modalContent={
          <>
            <ModalButton Icon={CheckCircleOutlined}>Resolve</ModalButton>
            <ModalButton Icon={EditOutlined}>Edit</ModalButton>
            <ModalButton Icon={EyeInvisibleOutlined}>Mark as unread</ModalButton>
            <ModalButton Icon={DeleteOutlined} className="colorRed" onClick={onDelete}>
              Delete
            </ModalButton>
          </>
        }
      />
      <UserInfo>
        <UserImage>
          <Avatar borderWidth={0} size={35} />
        </UserImage>
        <UserName>{createdAt}</UserName>
      </UserInfo>
      <InfoLabel>{formatTime(slideMillisec)}</InfoLabel> |{' '}
      <InfoLabel $isError={isPositionUnknown}>Slide {isPositionUnknown ? 'unknown' : slideId}</InfoLabel>
      <div>{richText}</div>
    </Root>
  );
};

export default forwardRef(CommentItem);
