import React, { useState, useCallback } from 'react';
import { ChangeEvent } from 'react';
import styled from 'styled-components';

import _Button from '~/shared/components/Button';
import Textarea from '~/shared/components/Textarea';
import ButtonsPanel from '~/shared/components/ButtonsPanel';

const Root = styled.div`
  display: flex;
  width: 100%;
  position: relative;
`;

const FormContainer = styled.div`
  width: 100%;
`;

const InputWrapper = styled.div`
  border-top: 1px solid ${({ theme }) => theme.colors.divider};
  padding: 20px 0;
  width: 90%;
  margin: 0 15px;
`;

const ButtonWrapper = styled.div`
  background: ${({ theme }) => theme.colors.backgroundLight};
  display: flex;
  padding: 20px;
  justify-content: space-between;
`;
const Button = styled(_Button)``;

const BottomText = styled.div`
  color: ${({ theme }) => theme.colors.textDark};
  font-size: 14px;
  padding: 3px 16px 0 0;
`;

const ErrorMessage = styled.span`
  width: 100%;
  color: ${({ theme }) => theme.colors.error};
`;

const Form = ({ onSubmit }: { onSubmit: (val: string) => void }) => {
  const [inputValue, setInputValue] = useState('');
  const [errorText, setErrorText] = useState('');
  const [isActiveInputWrapper, setActiveInputWrapper] = useState(false);

  const handleShowButtons = useCallback(() => {
    setActiveInputWrapper(true);
  }, [setActiveInputWrapper]);

  const handleCancelButtons = useCallback(() => {
    setActiveInputWrapper(false);
  }, [setActiveInputWrapper]);

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLTextAreaElement>) => {
      if (e.target.value) {
        setErrorText('');
      }
      setInputValue(e.target.value);
    },
    [setInputValue]
  );

  const handleSend = useCallback(() => {
    if (inputValue === '') {
      setErrorText('Enter message content to send');
    } else {
      onSubmit(inputValue);
      setInputValue('');
      setErrorText('');
      setActiveInputWrapper(false);
    }
  }, [inputValue, setActiveInputWrapper, setErrorText, onSubmit]);

  return (
    <Root>
      <FormContainer>
        <InputWrapper>
          <Textarea onChange={handleChange} onClick={handleShowButtons} value={inputValue} data-cy="commentTextArea" />
          <ErrorMessage>{errorText}</ErrorMessage>
          {isActiveInputWrapper && (
            <ButtonsPanel onCancelButtonClick={handleCancelButtons} onOkButtonClick={handleSend} />
          )}
        </InputWrapper>
        <ButtonWrapper>
          <BottomText>Invite your a colleagues to start a conversation</BottomText>
          <Button>Invite</Button>
        </ButtonWrapper>
      </FormContainer>
    </Root>
  );
};

export default Form;
