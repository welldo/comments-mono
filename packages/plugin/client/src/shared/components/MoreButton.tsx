import styled from 'styled-components';

export const MoreButton = styled.button`
  background: transparent;
  border-radius: 50%;
  border: none;
  outline: none;
  font-size: 18px;
  :hover {
    background: ${({ theme }) => theme.colors.backgroundHoverDarkness};
    color: ${({ theme }) => theme.colors.primary};
  }
`;

export default MoreButton;
