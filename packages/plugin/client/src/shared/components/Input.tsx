import styled from 'styled-components';

export const Input = styled.input`
  border: 1px solid ${({ theme }) => theme.colors.divider};
  padding: 8px 12px;
  width: 100%;
`;

export default Input;
