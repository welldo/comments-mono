import React, { ComponentPropsWithRef, ExoticComponent } from 'react';
import styled from 'styled-components';

interface AlignmentProp {
  isIconRight?: boolean;
}

const ModalListItem = styled.div<AlignmentProp & { selected?: boolean }>`
  display: flex;
  justify-content: ${({ isIconRight }) => (isIconRight ? 'space-between' : 'flex-start')};
  align-items: center;
  padding: 6px 12px;
  cursor: pointer;
  &:hover {
    background: ${({ theme }) => theme.colors.backgroundHover};
    border-radius: 8px;
    span {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
  &.colorRed {
    color: ${({ theme }) => theme.colors.error};
  }

  span {
    ${({ selected }) => (selected ? '' : '/*')}
    color: ${({ theme }) => theme.colors.primary};
    ${({ selected }) => (selected ? '' : '*/')}
  }
`;

const IconWrapper = styled.div<AlignmentProp>`
  margin-${({ isIconRight }) => (isIconRight ? 'left' : 'right')}: 10px;
`;

const TextWrapper = styled.span`
  font-size: 14px;
`;

const ModalButton = ({
  children,
  Icon,
  isIconRight,
  selected,
  ...rest
}: { Icon?: ExoticComponent; isIconRight?: boolean; selected?: boolean } & ComponentPropsWithRef<'div'>) => {
  return (
    <ModalListItem {...rest} isIconRight={isIconRight} selected={selected}>
      {!isIconRight && Icon && (
        <IconWrapper isIconRight={isIconRight}>
          <Icon />
        </IconWrapper>
      )}
      <TextWrapper>{children}</TextWrapper>
      {isIconRight && Icon && (
        <IconWrapper isIconRight={isIconRight}>
          <Icon />
        </IconWrapper>
      )}
    </ModalListItem>
  );
};

export default ModalButton;
