import styled from 'styled-components';

export const Button = styled.button`
  border: 1px solid ${({ theme }) => theme.colors.primary};
  border-radius: 25px;
  text-transform: capitalize;
  color: ${({ theme }) => theme.colors.primary};
  padding: 10px 15px;
  background: ${({ theme }) => theme.colors.white};

  :hover {
    background: ${({ theme }) => theme.colors.primary};
    color: ${({ theme }) => theme.colors.white};
  }
  :focus {
    outline: none;
  }
`;

export default Button;
