import styled from 'styled-components';
import React from 'react';

export const Wrapper = styled.div`
  position: absolute;
  right: 43px;
  left: auto;
  top: 55px;
  z-index: 999;
  box-shadow: 0 0 11px 0px ${({ theme }) => theme.colors.backgroundHover};
  border-radius: 10px;
`;

export const StyledModal = styled.div`
  z-index: 100;
  background: ${({ theme }) => theme.colors.white};
  position: relative;
  margin: auto;
  border-radius: 8px;
`;

export const Header = styled.div`
  border-radius: 8px 8px 0 0;
  display: flex;
  justify-content: space-between;
  padding-top: 10px;
`;

export const HeaderText = styled.div`
  color: ${({ theme }) => theme.colors.white};
  align-self: center;
  color: lightgray;
  padding-left: 20px;
`;

export const CloseButton = styled.button`
  font-size: 0.8rem;
  border: none;
  border-radius: 3px;
  margin-left: 0.5rem;
  background: none;
  :hover {
    cursor: pointer;
  }
`;

export const Content = styled.div`
  padding: 10px;
  max-height: 30rem;
  overflow-x: hidden;
  overflow-y: auto;
`;

export interface ModalProps {
  isShown: boolean;
  hide: () => void;
  modalContent: JSX.Element;
  headerText?: string;
  closeButton?: boolean;
  onClick?: () => void;
}

const Modal = ({ isShown, hide, closeButton, modalContent, headerText, onClick }: ModalProps) => {
  const modal = (
    <Wrapper onClick={onClick} data-cy="modal">
      <StyledModal>
        {headerText && (
          <Header>
            <HeaderText>{headerText}</HeaderText>
            {closeButton && <CloseButton onClick={hide}>X</CloseButton>}
          </Header>
        )}
        <Content>{modalContent}</Content>
      </StyledModal>
    </Wrapper>
  );

  // return isShown ? ReactDOM.createPortal(modal, document.body) : null;
  return isShown ? modal : null;
};

export default Modal;
