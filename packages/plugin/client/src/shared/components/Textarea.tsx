import styled from 'styled-components';

const Textarea = styled.textarea`
  border: 1px solid ${({ theme }) => theme.colors.divider};
  padding: 8px 12px;
  width: 100%;
  height: auto;
  min-height: 40px;
  border-radius: 5px;
  :hover {
    border-color: ${({ theme }) => theme.colors.primary};
  }

  :focus {
    outline-color: ${({ theme }) => theme.colors.primary};
  }
`;

export default Textarea;
