import styled from 'styled-components';

export const PageContentContainer = styled.div`
  background: #fff;
  margin: 24px 16px;
  padding: 24px;
  min-height: 280px;
  display: flex;
  position: relative;
`;

export const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;
