import { createContext } from 'react';

export const TickerContext = createContext(new Date());
