import { useRecoilState } from 'recoil';

import { activeMenu, isMenuActive } from '../recoil';

const useModal = () => {
  const [isShown, setIsShown] = useRecoilState(isMenuActive);
  const [menuId, setMenuId] = useRecoilState(activeMenu);

  const toggle = (id: string) => {
    if (menuId == id) setIsShown(!isShown);
    else setIsShown(true);
    setMenuId(id);
  };

  return {
    isShown,
    toggle,
    menuId,
  };
};

export default useModal;
