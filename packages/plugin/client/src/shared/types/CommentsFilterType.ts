enum CommentsFilterType {
  ALL = 'ALL',
  RESOLVED = 'RESOLVED',
  UNREAD = 'UNREAD',
  MENTIONED = 'MENTIONED',
}

export default CommentsFilterType;
