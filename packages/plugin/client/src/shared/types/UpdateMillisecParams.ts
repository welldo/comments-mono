export default interface UpdateMillisecParams {
  increment: number;
  start: number;
}
