enum ChangeInfo {
  INSERT = 'INSERT',
  REMOVE = 'REMOVE',
  MODIFY = 'MODIFY',
}

export default ChangeInfo;
