export const breakPoints = {
  small: '962px',
  medium: '1232px',
  big: '1494px',
};

export const mediaRules = {
  fromSmall: `(min-width: ${breakPoints.small})`,
  untilSmall: `(max-width: ${breakPoints.small})`,
  untilMedium: `(max-width: ${breakPoints.medium})`,
  untilBig: `(max-width: ${breakPoints.big})`,
};
