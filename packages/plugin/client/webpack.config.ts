import merge from 'webpack-merge';
import HtmlWebpackPlugin from 'html-webpack-plugin';

// eslint-disable-next-line @welldone-software/modules-engagement
import baseConfig from './base.webpack.config';

const mockStudioHostEnabled = process.env.REACT_APP_ENABLE_MOCK_STUDIO_HOST === 'true';

export default merge(baseConfig, {
  entry: mockStudioHostEnabled
    ? {
        host: './src/host-mock.tsx',
      }
    : {},

  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'index.html',
      inject: true,
      chunks: ['main'],
    }),
    ...(mockStudioHostEnabled
      ? [
          new HtmlWebpackPlugin({
            template: 'src/index.html',
            filename: 'host.html',
            inject: true,
            chunks: ['host'],
          }),
        ]
      : []),
  ],

  devServer: { port: 3000 },
} as any);
