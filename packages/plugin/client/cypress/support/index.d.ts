/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    getCommentsIframe(): Chainable<Element>;
    addComment(commentText: string): Chainable<Element>;
    addReply(commentIdx: number): Chainable<Element>;
    addReply(commentText: string): Chainable<Element>;
    getCommentByText(uniqueText: string): Chainable<Element>;
    cleanup(filtersToCleanCount: number): void;
    filterCommentsBy(filterIndex: number): void;
  }
}
