// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('getCommentsIframe', () => {
  return cy.get('iframe').its('0.contentDocument.body');
});

Cypress.Commands.add('addComment', (commentText: string): Cypress.Chainable<Element> => {
  const getFormContainer = () =>
    cy
      .getCommentsIframe()
      .find('[data-cy=form-wrapper]')
      .find('[data-cy=rich-text-form]')
      .find('[data-cy=form-container]')
      .then(cy.wrap);

  const emptyInputWrapper = getFormContainer().find('[data-cy=empty-input-wrapper]').then(cy.wrap);
  emptyInputWrapper.should('exist');
  emptyInputWrapper.click();

  const editor = getFormContainer().find('[data-cy=rich-text-editor]').then(cy.wrap);
  editor.should('exist');
  editor.click();

  editor.type(commentText);

  const postButton = getFormContainer().find('[data-cy=postButton]').then(cy.wrap);
  postButton.should('exist');
  postButton.click();

  cy.getCommentsIframe().contains(commentText);

  return cy
    .getCommentByText(commentText)
    .find('[data-cy=comment-time-from-creation]')
    .then(creationTimeElement => {
      expect(creationTimeElement.text()).eq('Moments ago');
    })
    .then(cy.wrap) as Cypress.Chainable<Element>;
});

Cypress.Commands.add('getCommentByText', (uniqueText: string): Cypress.Chainable<Element> => {
  return cy
    .getCommentsIframe()
    .find('[data-cy=comments]')
    .children()
    .then(comments => {
      for (let i = 0; i < comments.length; i++) {
        if (comments[i].innerText.match(uniqueText)) {
          return comments[i];
        }
      }
    }) as Cypress.Chainable<Element>;
});

const addReplyToComment = (
  commentGetter: () => Cypress.Chainable<JQuery<HTMLElement> | Element>
): Cypress.Chainable<Element> => {
  let comment = commentGetter();
  comment.should('exist');
  comment.find('[data-cy=comment-footer]').find('[data-cy=reply-button]').click();

  comment = commentGetter();
  const editor = comment.find('[data-cy=rich-text-editor]').then(cy.wrap);
  editor.should('exist');
  editor.click();
  const randomReply = `test reply ${Math.floor(Math.random() * 23454666) + 1}`;
  editor.type(randomReply);

  comment = commentGetter();
  const postButton = comment.find('[data-cy=postButton]').then(cy.wrap);
  postButton.should('exist');
  postButton.click();

  return cy.getCommentsIframe().contains(randomReply);
};

Cypress.Commands.add('addReply', (commentIdx: number): Cypress.Chainable<Element> => {
  const commentGetter = () => cy.getCommentsIframe().find('[data-cy=comments]').children().eq(commentIdx);
  return addReplyToComment(commentGetter);
});

Cypress.Commands.add('addReply', (commentText: string): Cypress.Chainable<Element> => {
  const commentGetter = () => cy.getCommentByText(commentText);
  return addReplyToComment(commentGetter);
});

const removeFromIfItsTestComment = (filtersToCleanCount: number, currentIdx: number, filterElementIdx: number) => {
  cy.getCommentsIframe()
    .find('[data-cy=comments]')
    .children()
    .then(comments => {
      if (comments.length > currentIdx) {
        cy.wrap(comments[currentIdx]).then(comment => {
          if (comment.text().match(/test [a-zA-Z]+ [0-9]+/)) {
            cy.getCommentsIframe()
              .find('[data-cy=comments]')
              .children()
              .eq(currentIdx)
              .find('[data-cy=comment-more-button]')
              .children()
              .children()
              .eq(0)
              .click({ force: true });
            cy.wait(500);
            cy.getCommentsIframe().find('[data-cy=comment-delete]').click({ force: true });
            cy.wait(500);
            removeFromIfItsTestComment(filtersToCleanCount, currentIdx, filterElementIdx);
          } else {
            removeFromIfItsTestComment(filtersToCleanCount, currentIdx + 1, filterElementIdx);
          }
        });
      } else if (filterElementIdx < filtersToCleanCount) {
        removeFromIfItsTestComment(filtersToCleanCount, 0, filterElementIdx + 1);
      }
    });
};

Cypress.Commands.add('filterCommentsBy', (filterIdx: number) => {
  cy.getCommentsIframe().find('.vcp-dropdown-select').click();
  cy.wait(400);
  cy.getCommentsIframe()
    .find('[data-id="contentDataId"]')
    .find('.vcp-dropdown-select__options-list')
    .find('.vcp-dropdown-select__option')
    .find('.vcp-dropdown-select__option__content')
    .children()
    .eq(filterIdx)
    .click();
});

Cypress.Commands.add('cleanup', (filtersToCleanCount: number): void => {
  cy.wait(1000);
  cy.filterCommentsBy(0);
  cy.wait(1000);
  removeFromIfItsTestComment(filtersToCleanCount, 0, 0);
});
