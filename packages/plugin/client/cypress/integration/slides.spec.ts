// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../support/index.d.ts" />

describe('slides', () => {
  const randomSlideName = `test slide ${Math.floor(Math.random() * 23454666) + 1}`;

  beforeEach(() => {
    cy.viewport(1980, 1024);
    cy.visit(Cypress.env('base_url'));
    cy.wait(1000);
    cy.reload();
    cy.getCommentsIframe().should('exist');
    cy.wait(3000);
  });

  it('add new slide', () => {
    cy.get('[data-cy=add-new-slide-button]').click();

    cy.get('[data-cy=slide-name-input]').type(randomSlideName);
    cy.get('.ant-modal-content').find('.ant-modal-footer').find('button').children().eq(1).click();

    cy.get('[data-cy=slides]').contains(randomSlideName);
  });

  it('switch to the new (the last) slide', () => {
    cy.getCommentsIframe()
      .find('[data-cy=plugin-slide-id]')
      .then(slideIdElem => {
        const oldSlideId = slideIdElem.text();

        cy.get('[data-cy=slides]')
          .children()
          .then(slides => {
            cy.wrap(slides)
              .eq(slides.length - 1)
              .trigger('click')
              .click();
            cy.getCommentsIframe()
              .find('[data-cy=plugin-slide-id]')
              .then(slideIdElemAgain => {
                const newSlideId = slideIdElemAgain.text();
                expect(newSlideId).not.eq(oldSlideId);
              });
          });
      });
  });

  it('add comment to the inserted (last) slide', () => {
    cy.get('[data-cy=slides]')
      .children()
      .then(slides => {
        cy.wrap(slides)
          .eq(slides.length - 1)
          .click();

        cy.wait(500);

        const commentText = `test comment ${Math.floor(Math.random() * 23454666) + 1}`;
        cy.addComment(commentText);
        cy.wait(1000);
        cy.getCommentsIframe().find('[data-cy=comments]').contains(commentText);

        cy.getCommentsIframe()
          .find('[data-cy=comments]')
          .children()
          .then(comments => {
            for (let i = 0; i < comments.length; i++) {
              if (comments[i].innerText.match(commentText)) {
                cy.getCommentsIframe()
                  .find('[data-cy=comments]')
                  .children()
                  .eq(i)
                  .find('[data-cy=comment-slide-nr]')
                  .then(slideNrElem => {
                    const slideNr = parseInt(slideNrElem.text().split(' ')[1]);
                    expect(slideNr).eq(slides.length);
                  });
              }
            }
          });

        cy.cleanup(0);
      });
  });

  after(() => {
    cy.get('[data-cy=slides]')
      .children()
      .then(slides => {
        cy.wrap(slides)
          .eq(slides.length - 1)
          .find('[data-cy=delete-slide-button]')
          .trigger('click')
          .click();
        cy.wait(500);
        cy.get('[data-cy=slides]').contains(randomSlideName).should('not.exist');
      });
  });
});
