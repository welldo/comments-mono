// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../support/index.d.ts" />

describe('comments and replies', () => {
  const RESOLVED_COMMENTS_FILTER = 1;
  // const UNREAD_COMMENTS_FILTER = 2;
  // const getComments = () => cy.getCommentsIframe().find('[data-cy=comments]').then(cy.wrap);
  const clickDeleteButton = () => cy.getCommentsIframe().find('[data-cy=comment-delete]').click({ force: true });
  const clickEditButton = () => cy.getCommentsIframe().find('[data-cy=comment-edit]').click({ force: true });
  const clickResolveButton = () => cy.getCommentsIframe().find('[data-cy=comment-resolve]').click({ force: true });
  // const clickReadButton = () => cy.getCommentsIframe().find('[data-cy=comment-mark-read]').click({ force: true });
  const getRandomNumber = () => Math.floor(Math.random() * 23454666) + 1;
  const getRandomCommentText = () => `test comment ${getRandomNumber()}`;

  beforeEach(() => {
    cy.viewport(1980, 1024);
    cy.visit(Cypress.env('base_url'));
    cy.wait(2000);
    cy.getCommentsIframe().should('exist');
  });

  describe('add, edit, resolve & remove comment', () => {
    const commentText = getRandomCommentText();

    it('add a comment', () => {
      cy.addComment(commentText);
    });

    it('edit a comment', () => {
      let comment = cy.getCommentByText(commentText);
      comment.should('exist');
      comment.find('[data-cy=comment-header]').should('exist');
      comment.find('[data-cy=comment-more-button]').children().children().eq(0).click({ force: true });
      clickEditButton();
      comment = cy.getCommentByText(commentText);
      const editor = comment.find('[data-cy=rich-text-editor]').then(cy.wrap);
      editor.should('exist');
      editor.click();
      const editedText = getRandomCommentText();
      editor.clear();
      editor.type(editedText);

      comment = cy.getCommentByText(commentText);
      const updateButton = comment.find('[data-cy=postButton]').then(cy.wrap);
      updateButton.should('exist');
      updateButton.click();

      cy.getCommentsIframe().contains(editedText).should('exist');
    });

    it('resolve a comment', () => {
      const resolvedText = `test resolved ${getRandomNumber()}`;
      cy.addComment(resolvedText);
      cy.wait(1000);
      const resolvedComment = cy.getCommentByText(resolvedText);
      resolvedComment.should('exist');
      resolvedComment.find('[data-cy=comment-header]');
      resolvedComment.find('[data-cy=comment-more-button]').children().children().eq(0).click({ force: true });
      clickResolveButton();
      cy.getCommentsIframe().contains(resolvedText).should('not.exist');
      cy.filterCommentsBy(RESOLVED_COMMENTS_FILTER);
      cy.getCommentByText(resolvedText).should('exist');

      cy.getCommentByText(commentText)
        .find('[data-cy=comment-more-button]')
        .children()
        .children()
        .eq(0)
        .click({ force: true });
      clickDeleteButton();
    });

    // it('mark comment as read', () => {
    //   const readText = `test read ${getRandomNumber()}`;
    //   cy.addComment(readText);
    //   cy.wait(1000);
    //   const readComment = cy.getCommentByText(readText);
    //   readComment.should('exist');
    //   readComment.find('[data-cy=comment-header]');
    //   readComment.find('[data-cy=comment-more-button]').children().children().eq(0).click({ force: true });
    //   clickReadButton();
    //   cy.getCommentsIframe().contains(readText).should('exist');
    //   cy.wait(1000);
    //   cy.filterCommentsBy(UNREAD_COMMENTS_FILTER);
    //   cy.getCommentsIframe().contains(readText).should('not.exist');
    // });
  });

  describe('replies', () => {
    it('adds a reply to the inserted comment', () => {
      const commentText = getRandomCommentText();
      cy.addComment(commentText);
      cy.wait(500);

      cy.addReply(commentText).should('exist');
    });

    /**
    describe('reply of reply', () => {
      const commentText = getRandomCommentText();
      const repyText = getRandomReplyText();

      it("adds a reply to the inserted comment's firstly inserted reply", () => {
        cy.addComment(commentText);
        cy.wait(1000);
        cy.addReply(commentText);
        cy.wait(1000);

        let comment = cy.getCommentByText(commentText);
        let reply = getCommentsReplyAt(comment, 0);
        reply.should('exist');
        reply.find('[data-cy=reply-button]').click();

        comment = cy.getCommentByText(commentText);
        reply = getCommentsReplyAt(comment, 0);
        const editor = reply.find('[data-cy=rich-text-editor]').then(cy.wrap);
        editor.should('exist');
        editor.click();
        editor.type(repyText);

        comment = cy.getCommentByText(commentText);
        reply = getCommentsReplyAt(comment, 0);
        const postButton = reply.find('[data-cy=postButton]').then(cy.wrap);
        postButton.should('exist');
        postButton.click();

        cy.getCommentsIframe().contains(repyText);
      });

      it("removes the reply of the inserted comment's firstly inserted reply", () => {
        let comment = cy.getCommentByText(commentText);
        comment.find('[data-cy=show-replies-button]').click();

        comment = cy.getCommentByText(commentText);
        const reply = getCommentsReplyAt(comment, 0);
        reply.find('[data-cy=comment-header]').find('[data-cy=comment-more-button]').children().eq(1).click();

        comment = cy.getCommentByText(commentText);
        reply.find('[data-cy=modal]').find('[data-cy=modal-buttons]').children().eq(deleteIdx).click();

        cy.getCommentsIframe().contains(repyText).should('not.exist');
      });
    });
    */
  });

  after(() => {
    cy.cleanup(1);
  });
});
