// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../support/index.d.ts" />

describe('incrementing and decrementing seconds', () => {
  const getComments = () => cy.getCommentsIframe().find('[data-cy=comments]').then(cy.wrap);
  const getCommentAt = (index: number) => getComments().children().eq(index).then(cy.wrap);
  const setCurrentTimeOnSlide = (second: number) =>
    cy.get('[data-cy=powtoon-footer]').find('.ant-slider-dot').eq(second).click();

  beforeEach(() => {
    cy.viewport(1980, 1024);
    cy.visit(Cypress.env('base_url'));
    cy.getCommentsIframe().should('exist');
    cy.wait(3000);
    cy.addComment(`test comment ${Math.floor(Math.random() * 23454666) + 1}`);
  });

  // const inputs = [true, false];
  const inputs = []; // seconds no longer needed or supported now.

  const getSecondsTexts = (secondsElements: JQuery<HTMLElement>) => {
    const result: string[] = [];
    return cy
      .wrap(secondsElements)
      .each(seconds => {
        result.push(seconds.text());
      })
      .then(() => result);
  };

  const clickPlusMinusButton = (increment: boolean) => {
    const buttonId = `[data-cy=${increment ? 'increment' : 'decrement'}-sec-button]`;
    if (!increment) {
      setCurrentTimeOnSlide(2);
    }
    cy.get('[data-cy=powtoon-footer]').find(buttonId).should('exist');
    cy.get('[data-cy=powtoon-footer]').find(buttonId).click();
    cy.pause();
  };

  const compareTimes = (oldTime: string, newTime: string, isIncrement: boolean, idx: number) => {
    const [oldMins, oldSecs] = oldTime.split(':').map(value => parseInt(value));
    const [newMins, newSecs] = newTime.split(':').map(value => parseInt(value));
    const addition = isIncrement ? -1 : 1;

    const oldSecToBe = isIncrement ? 59 : 0;
    const newSecToBe = isIncrement ? 0 : 59;

    // we check whether the new time is greater or less exactly with 1 than the old time
    if (
      !(
        (newSecs + addition === oldSecs && newMins === oldMins) ||
        (newMins + addition === oldMins && newSecs === newSecToBe && oldSecs === oldSecToBe) ||
        (oldMins === newMins && oldMins === 0 && oldSecs === newSecs && oldSecs <= 1)
      )
    ) {
      throw `wrong seconds value at the ${idx}. comment`;
    }
  };

  inputs.forEach(isIncrementing => {
    it(`${isIncrementing ? 'increment' : 'decrement'} millisec`, () => {
      cy.getCommentsIframe()
        .find('[data-cy=comment-sec]')
        .then(commentsSecs => {
          getSecondsTexts(commentsSecs).then(oldValues => {
            clickPlusMinusButton(isIncrementing);

            oldValues.map((oldCommentSecs, idx) => {
              getCommentAt(idx)
                .find('[data-cy=comment-sec]')
                .then(updatedCommentSecs => {
                  compareTimes(oldCommentSecs, updatedCommentSecs.text(), isIncrementing, idx);
                });
            });
          });
        });
    });
  });

  after(() => {
    cy.cleanup(1);
  });
});

export {};
