// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../support/index.d.ts" />

const RESOLVED_COMMENTS_FILTER = 1;
describe('filtering', () => {
  beforeEach(() => {
    cy.viewport(1980, 1024);
    cy.visit(Cypress.env('base_url'));
    cy.getCommentsIframe().should('exist');
    cy.wait(3000);
  });

  it('filtering resolved comments', () => {
    const getCurrentFilter = () =>
      cy
        .getCommentsIframe()
        .find('.vcp-dropdown-select')
        .invoke('text')
        .then(currentFilter => currentFilter);

    getCurrentFilter().then(oldCurrentFilter => {
      cy.filterCommentsBy(RESOLVED_COMMENTS_FILTER);
      cy.wait(1000);
      getCurrentFilter().then(newCurrentFilter => expect(newCurrentFilter).not.eq(oldCurrentFilter));
    });
    // TODO: after the resolve comment feature will work we can test whether a comment is displayed as a resolved one
  });
});
