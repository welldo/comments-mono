## Core Api

Interfaces for core, low level messaging layer.

## Powtoon Apis

Interfaces for powtoon model, events etc.

## Adapters (adapts core messages to api calls)

Implementation classes and functions exposing the apis on top a given core implementation


## Core/Zoid

Implements core api


