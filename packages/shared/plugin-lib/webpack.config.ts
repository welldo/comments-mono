import * as path from 'path';

import { CleanWebpackPlugin } from 'clean-webpack-plugin';

const posixPath = (path: string) => path;
const resolve = (...args: string[]) => posixPath(path.resolve(...args));
const srcDir = resolve(__dirname, 'src');
const outDir = resolve(__dirname, 'dist');

module.exports = () => {
  const plugins: any[] = [new CleanWebpackPlugin()];

  return {
    externals: {
      react: 'react',
      'react-dom': 'react-dom',
    },
    mode: 'production',
    target: 'node',
    entry: resolve(srcDir, 'index.ts'),
    devtool: 'source-map',
    optimization: {
      minimize: false,
    },
    module: {
      rules: [
        {
          test: /\.ts|\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js'],
      mainFields: ['main'],
    },
    output: {
      libraryTarget: 'commonjs',
      filename: 'index.js',
      path: outDir,
    },
    plugins: plugins,
  };
};
