import {
  ApiName,
  PluginBase,
  BadgePresenter,
  MenuPresenter,
  NavigationPresenter,
  PowtoonModelEventsProvider,
  PowtoonUiEventsProvider,
} from './common';

export type StudioModelEvent =
  // /**
  //  * Slide list ready.
  //  * Receive slides in the right order, with id and duration.
  //  */
  | {
      name: 'ready';
      powtoonId: string;
      userId: string;
    }
  /**
   * A slide added/removed/deleted/reordered or initialized
   */
  | { name: 'slides'; slides: { id: string; duration_millisec: number }[] }
  /**
   * A slide time slice added/removed
   */
  | {
      name: 'slideTime';
      id: string;
      action: 'add' | 'remove';
      startMillisec: number;
      durationMillisec: number;
    };

export type StudioUiEvent =
  | { name: 'initialize' }
  | { name: 'shutdown' }
  | { name: 'pause' }
  /**
   * Timeline head has changed
   */
  | { name: 'selection'; target: StudioTarget }
  /**
   * Timeline head has changed
   */
  | { name: 'timeline'; millisec: number };

export type DialogEvents =
  /**
   * Request Invite UI
   */
  { name: 'invite'; event: PluginUIEvent };

export type NotificationType = 'commenting.comment ' | 'commenting.reply' | 'commenting.mention';

export type NotificationDescriptor = {
  notificationId: string;
  eventId: string;
  notificationType: NotificationType;
  commentTarget: { type: 'Slide'; id: string; slideMillisec?: number };
  commenterId: string;
  recipients?: string[] | Set<string>;
  header?: string;
  body?: string;
  twit?: string;
};

export interface StudioTarget {
  type: 'slideThumb' | 'asset';
  id: string; // multiple - array|glob|url|comma delimited?
}

export interface PluginUIEvent {
  userId?: string;
}

/**
 * The powtoon studio from the plugin point of view
 */
export interface PowtoonStudio
  extends ApiName<'PowtoonStudio'>,
    PluginBase,
    BadgePresenter<StudioTarget>,
    MenuPresenter<StudioTarget>,
    NavigationPresenter<StudioTarget>,
    PowtoonModelEventsProvider<StudioModelEvent>,
    PowtoonUiEventsProvider<StudioUiEvent> {}
