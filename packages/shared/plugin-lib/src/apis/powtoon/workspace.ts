import { BadgePresenter, MenuPresenter } from './common';

export interface WorkspaceTarget {
  type: 'powtoonThumb' | 'powtoonGroup';
  id: string; //glob?
}

export interface PowtoonWorkspace extends BadgePresenter<WorkspaceTarget>, MenuPresenter<WorkspaceTarget> {}
