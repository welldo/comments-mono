import { NotificationDescriptor } from './studio';

// MENU SUPPORT (NEXT VERSION)
export interface StaticMenuItem {
  id: string; // command id
  caption: string;
  icon: string;
}

export interface StaticMenuSpec {
  type: 'static';
  data: StaticMenuItem[];
}

export interface CallbackMenuSpec {
  type: 'callback';
}

export type MenuSpec = CallbackMenuSpec | StaticMenuSpec;

export interface MenuPresenter<TTarget> {
  setMenu(arg: { target: TTarget; menu: MenuSpec }): void;
}

export interface MenuPresenterHandler<TTarget> {
  onSetMenu: MenuPresenter<TTarget>['setMenu'];
}

// BADGE SUPPORT
export interface BadgeSpec {
  type: 'text';
  data: string;
}

export interface BadgePresenter<TTarget> {
  setBadge(arg: { target: TTarget; badge: BadgeSpec }): void;
}

export interface BadgePresenterHandler<TTarget> {
  onSetBadge: BadgePresenter<TTarget>['setBadge'];
}

export interface NavigationPresenter<TTarget> {
  navigateToTarget(arg: { target: TTarget }): void;
}

export interface NavigationPresenterHandler<TTarget> {
  onNavigateToTarget: NavigationPresenter<TTarget>['navigateToTarget'];
}

export interface PowtoonAccessToken {
  powtoonId: string;
  userId: string;
  permissions: string[];
  expiration: number;
}

export interface PluginBase {
  ready(): void;
  pause(): void;
  getDataHandler(method: string, args: any): Promise<any>;
  getPowtoonAccessToken(): Promise<any>;
  notifyUsers(args: NotificationDescriptor): void;
  openShareDialog(args?: any): void;
}

export interface PluginBaseHandler {
  onReady: PluginBase['ready'];
  onGetDataHandler: PluginBase['getDataHandler'];
  onNotifyUsers: PluginBase['notifyUsers'];
  onOpenShareDialog: PluginBase['openShareDialog'];
  onPause: PluginBase['pause'];
}

export interface EventBase {
  name: string;
  source?: string;
}

// EVENTS SUPPORT
export interface PowtoonModelEventsProvider<TModelEvent extends EventBase> {
  registerModelEventHandler(handler: (event: TModelEvent) => void): void;
}

// EVENTS SUPPORT
export interface PowtoonUiEventsProvider<TUiEvent extends EventBase> {
  registerUiEventHandler(handler: (event: TUiEvent) => void): void;
}

export interface ApiName<Name> {
  apiName: Name;
}

export interface PowtoonUser {
  id: string;
  firstName: string;
  lastName: string;
  thumbUrl: string;
}
