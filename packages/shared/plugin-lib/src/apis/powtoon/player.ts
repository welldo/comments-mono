import { PowtoonUiEventsProvider } from './common';

export type PlayerUiEvent =
  | { name: 'initialize' }
  | { name: 'shutdown' }
  | { name: 'started'; millisec: number }
  | { name: 'stopped'; millisec: number };

export type PowtoonPlayer = PowtoonUiEventsProvider<PlayerUiEvent>;
