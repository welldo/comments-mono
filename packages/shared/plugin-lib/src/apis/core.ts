export interface CoreMessage {
  apiName: string;
  methodName: string;
  arg: any;
  // correlationId?: string;
}

export interface CoreMessagingProvider {
  send(msg: CoreMessage): void;
  sendReceive(msg: CoreMessage): Promise<any>;
  setReceiver(handler: CoreMessageHandler): void;
}

export type CoreMessageHandler = CoreMessagingProvider['send'];
export type CorePromiseMessageHandler = CoreMessagingProvider['sendReceive'];
