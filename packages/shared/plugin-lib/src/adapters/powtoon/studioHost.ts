import { ApiName, StudioTarget, StudioModelEvent, StudioUiEvent } from '../..';
import { BadgePresenterHandler, CoreMessagingProvider, MenuPresenterHandler } from '../../apis';

/**
 * The powtoon studio plugin from the host point of view
 */
export class PowtoonStudioHost implements ApiName<'PowtoonStudio'> {
  apiName: 'PowtoonStudio' = 'PowtoonStudio';

  constructor(
    private readonly messaging: CoreMessagingProvider,
    readonly menuHandler: MenuPresenterHandler<StudioTarget>,
    readonly badgeHandler: BadgePresenterHandler<StudioTarget>
  ) {
    messaging.setReceiver(msg => {
      if (msg.apiName === this.apiName) {
        if (msg.methodName === 'setBadge') {
          return badgeHandler.onSetBadge(msg.arg);
        }
        if (msg.methodName === 'setMenu') {
          return menuHandler.onSetMenu(msg.arg);
        }
      }
    });
  }

  notifyUiEvent(event: StudioUiEvent): void {
    this.send('StudioUiEvent', event);
  }
  notifyModelEvent(event: StudioModelEvent): void {
    this.send('StudioModelEvent', event);
  }

  private send(methodName: string, arg: any) {
    this.messaging.send({ apiName: this.apiName, methodName, arg });
  }
}
