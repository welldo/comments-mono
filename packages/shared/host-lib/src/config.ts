export const apolloClientUri =
  process.env.REACT_APP_APOLLO_CLIENT_URI || 'https://right-sheepdog-77.hasura.app/v1/graphql';
export const apolloWsUri = process.env.REACT_APP_APOLLO_WS_URI || `wss://right-sheepdog-77.hasura.app/v1/graphql`;
