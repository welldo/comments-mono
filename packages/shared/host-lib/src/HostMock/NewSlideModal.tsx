import React from 'react';
import { Modal, Input, InputNumber, Form } from 'antd';

export default function NewSlideModal({
  visible,
  onCancel,
  onOk,
  name,
  duration,
  onDurationChange,
  onNameChange,
}: {
  visible: boolean;
  onCancel: () => void;
  onOk: () => void;
  onNameChange: (value: string) => void;
  onDurationChange: (value: number) => void;
  name: string;
  duration: number;
}) {
  return (
    <Modal title="Add new slide" visible={visible} onCancel={onCancel} onOk={onOk}>
      <Form layout="vertical">
        <Form.Item label="Slide Name">
          <Input value={name} onChange={evt => onNameChange(evt.target.value)} data-cy="slide-name-input" />
        </Form.Item>
        <Form.Item label="Slide Duration in sec">
          <InputNumber value={duration} onChange={val => onDurationChange(val as number)} step={5} />
        </Form.Item>
      </Form>
    </Modal>
  );
}
