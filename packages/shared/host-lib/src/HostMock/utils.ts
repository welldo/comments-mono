import { range, omit } from 'lodash';

import { Slide } from './recoils';

export const millisecToSec = (millisec: number) => millisec / 1000;
export const secToMillisec = (sec: number) => sec * 1000;

export const createDefaultSlide = () =>
  ({
    display_name: 'New slide',
    duration_millisec: secToMillisec(10),
    position: 0,
  } as Slide);

export const getStep = (num: number) => {
  switch (true) {
    case num >= 100:
      return 20;
    case num >= 50:
      return 10;
    case num >= 20:
      return 5;
    default:
      return 1;
  }
};

export const getSliderMarks = (millisec = 10000) => {
  const seconds = millisecToSec(millisec);
  const marks: Record<string, string> = {};
  const step = getStep(seconds);
  for (let i = 0; i < seconds; i += step) {
    marks[i] = `${i} sec`;
  }
  return marks;
};

export const slideGqlFields = `
  id
  display_name
  duration_millisec
  position
`;

export const getSlidePositionIncrement = (newPosition: number, oldPosition: number) =>
  newPosition < oldPosition ? 1 : -1;

export const getSlidesPositionRange = (newPosition: number, oldPosition: number) =>
  newPosition < oldPosition ? range(newPosition, oldPosition) : range(oldPosition + 1, newPosition + 1);

export const isNotifyPlugin = (oldSlides: Slide[], newSlides: Slide[]) =>
  oldSlides.length !== newSlides.length || oldSlides.some(({ id }, i) => id !== newSlides[i].id);

export const patchSlidesWithBadges = (prevSlides: Slide[], newSlides: Slide[]) =>
  newSlides.map(slide => {
    const prevSlide = prevSlides.find(({ id }) => id === slide.id);
    return {
      ...omit(slide, '__typename'),
      badges: prevSlide?.badges,
    };
  });
