import { sign } from 'jsonwebtoken';

const POWTOON_AUTH_ALG = 'HS512'; // HMAC using SHA-512 hash algorithm
const POWTOON_AUTH_SECRET = '6DAF318621F33431CFDD829E48378'; // fixed token
const POWTOON_AUTH_EXP = 60 * 60 * 60; // set for 60 days
const POWTOON_AUTH_ISSUER = 'powtoon.com';

const token = sign(
  {
    userId: '1',
    powtoonID: '',
    permissions: ['can_add_comments', 'can_view_comments', 'can_resolve_comments'],
    expiration: POWTOON_AUTH_EXP,
  },
  POWTOON_AUTH_SECRET,
  {
    issuer: POWTOON_AUTH_ISSUER,
    algorithm: POWTOON_AUTH_ALG,
    expiresIn: POWTOON_AUTH_EXP,
  }
);

export function getToken() {
  return token;
}
