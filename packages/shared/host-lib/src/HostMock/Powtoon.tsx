import React, { ReactNode, useCallback, useEffect, useState } from 'react';
import { Layout as _Layout, Card, Slider as _Slider, Button as _Button } from 'antd';
import styled from 'styled-components';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';
import { isNil } from 'lodash';

import Slides from './Slides';
import { Slide, Powtoon as PowtoonType } from './recoils';
import {
  getSlidePositionIncrement,
  getSlidesPositionRange,
  getSliderMarks,
  millisecToSec,
  secToMillisec,
} from './utils';
import useSlideMutations from './useSlideMutations';
import HeaderPart from './HeaderPart';

const { Header: _Header, Footer: _Footer, Sider, Content } = _Layout;

const Layout = styled(_Layout)`
  height: 100vh;
`;

const ContentLayout = styled(_Layout)`
  height: 100%;
`;

const Header = styled(_Header)`
  color: white;
`;

const Footer = styled(_Footer)`
  display: flex;
  justify-content: center;
`;

const PluginSider = styled(Sider)`
  width: 400px;
  margin-left: auto;
  height: 100%;
  .ant-layout-sider-children > div {
    height: 100%;
  }
`;

const SlidesSider = styled(Sider)`
  padding: 25px;
  overflow: auto;
`;

const CurrentSlideCard = styled(Card)`
  width: 100%;
  height: 100%;
`;

const SliderFooter = styled.div`
  display: flex;
  width: 1000px;
  & .ant-slider-mark-text {
    font-size: 12px;
  }
`;

const FooterButtonsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 10px;
`;

const Slider = styled(_Slider)`
  flex: 1;
`;

const Button = styled(_Button)`
  padding: 4px 8px;
`;

const ContentWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

interface Props {
  children: ReactNode;
  slides: Slide[];
  powtoons?: PowtoonType[];
  currentSlide?: Slide;
  currentPowtoonId?: string;
  setCurrentPowtoonId: (id: string) => void;
  createNewPowtoon: () => void;
  loading?: boolean;
  setCurrentSlide: (slide: Slide) => void;
  setReorderedSlides: (slides: Slide[]) => void;
  onDurationChange: (action: 'add' | 'remove', value: number) => void;
  onTimelineChange: (millisec: number) => void;
  addSlide: (slide: Slide) => void;
}

export default function Powtoon({
  children,
  slides,
  powtoons,
  currentSlide,
  currentPowtoonId,
  setCurrentPowtoonId,
  createNewPowtoon,
  loading,
  setCurrentSlide,
  setReorderedSlides,
  onDurationChange,
  onTimelineChange,
  addSlide,
}: Props) {
  const [sliderValueSeconds, setSliderValueSeconds] = useState(0);

  const {
    deleteSlideOnServer,
    mutateSlideDurationOnServer,
    mutateSlidesOrderOnServer,
    loading: mutationLoading,
  } = useSlideMutations(currentPowtoonId as string, currentSlide?.id);

  const updateSlidesOrder = ({
    newPosition,
    incValue,
    newRange,
    slideId,
  }: {
    newPosition: number;
    slideId: string;
    newRange: Array<number>;
    incValue: number;
  }) =>
    mutateSlidesOrderOnServer({
      variables: {
        newPos: newPosition,
        slideId,
        range: newRange,
        incValue,
        powtoonId: currentPowtoonId,
      },
    });

  useEffect(() => {
    if (currentSlide?.id) {
      setSliderValueSeconds(0);
    }
  }, [currentSlide?.id]);

  useEffect(() => {
    if (slides.length) {
      setCurrentSlide(slides.find(({ id }) => id === currentSlide?.id) || slides[0]);
    }
  }, [slides]);

  useEffect(() => {
    onTimelineChange(sliderValueSeconds);
  }, [sliderValueSeconds]);

  const changeDurationValueMilliseconds = (valueSeconds: number) => {
    const newValueSeconds = sliderValueSeconds + valueSeconds;
    if (newValueSeconds <= 0) return;

    setSliderValueSeconds(newValueSeconds);
    const updatedMillisec = currentSlide!.duration_millisec + secToMillisec(valueSeconds);
    mutateSlideDurationOnServer({
      variables: {
        duration: updatedMillisec,
      },
    });
    onDurationChange(valueSeconds >= 0 ? 'add' : 'remove', newValueSeconds);
  };

  return (
    <Layout>
      <Header>
        <HeaderPart
          powtoons={powtoons}
          setCurrentPowtoonId={setCurrentPowtoonId}
          createNewPowtoon={createNewPowtoon}
          currentPowtoonId={currentPowtoonId}
        />
      </Header>
      <Layout>
        <SlidesSider>
          <Slides
            currentSlideId={currentSlide?.id}
            slides={slides}
            onSlideClick={setCurrentSlide}
            createSlide={addSlide}
            onSlidesOrderChange={({ oldPosition, newPosition, slideId, reorderedSlides }) => {
              const newRange = getSlidesPositionRange(newPosition, oldPosition);
              const incValue = getSlidePositionIncrement(newPosition, oldPosition);
              updateSlidesOrder({ newPosition, slideId, newRange, incValue });
              setReorderedSlides(
                reorderedSlides.map((slide, i) => ({
                  ...slide,
                  position: i,
                }))
              );
            }}
            deleteSlide={(slide: Slide) =>
              deleteSlideOnServer({
                variables: {
                  slideId: slide.id,
                  slidePos: slide.position,
                },
              })
            }
          />
        </SlidesSider>
        <Content>
          <ContentLayout>
            <Content>
              <CurrentSlideCard title={currentSlide?.display_name}>
                <ContentWrapper>
                  <p>Currently selected value: {secToMillisec(sliderValueSeconds || 0)} milliseconds</p>
                </ContentWrapper>
              </CurrentSlideCard>
            </Content>
            <PluginSider width={400}>{children}</PluginSider>
          </ContentLayout>
        </Content>
      </Layout>
      <Footer data-cy="powtoon-footer">
        <SliderFooter>
          <Slider
            min={0}
            value={sliderValueSeconds}
            max={isNil(currentSlide?.duration_millisec) ? 10 : millisecToSec(currentSlide!.duration_millisec)}
            marks={getSliderMarks(currentSlide?.duration_millisec)}
            onChange={(val: number) => {
              onTimelineChange(val);
              setSliderValueSeconds(val);
            }}
          />
          <FooterButtonsWrapper>
            <Button
              disabled={loading}
              onClick={() => changeDurationValueMilliseconds(1)}
              data-cy="increment-sec-button"
            >
              <PlusOutlined />
            </Button>
            <Button
              disabled={loading || mutationLoading}
              onClick={() => changeDurationValueMilliseconds(-1)}
              data-cy="decrement-sec-button"
            >
              <MinusOutlined />
            </Button>
          </FooterButtonsWrapper>
        </SliderFooter>
      </Footer>
    </Layout>
  );
}
