// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { atom, selector, useRecoilState, useRecoilValue } from 'recoil';

export interface Powtoon {
  display_name: string;
  id: string;
  slides_aggregate: { nodes: Slide[] };
}
export interface Slide {
  id: string;
  duration_millisec: number;
  display_name: string;
  position: number;
  badges?: number;
}

export const slidesState = atom<Slide[]>({
  key: 'slides',
  default: [],
});

export const currentSlideState = atom<Slide | undefined>({
  key: 'currentSlide',
  default: undefined,
});

export const currentPowtoonIdState = atom<string | undefined>({
  key: 'currentPowtoonId',
  default: undefined,
});
