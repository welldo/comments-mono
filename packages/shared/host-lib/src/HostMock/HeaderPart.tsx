import React, { useEffect, useState } from 'react';
import Dropdown, { Option } from 'react-dropdown';
import styled from 'styled-components';

import { Powtoon as PowtoonType } from './recoils';

import 'react-dropdown/style.css';

const HeaderWrapper = styled.div`
  display: flex;
  align-items: center;

  .headerTitle {
    width: 150px;
  }
`;

const AddButton = styled.button`
  border: 1px solid white;
  border-radius: 3px;
  height: 40px;
  width: 60px;
  background-color: transparent;
  text-align: center;
  line-height: 40px;
  color: white;

  :hover {
    cursor: pointer;
    background-color: white;
    color: #00152a;
  }
`;

const PowtoonChooserWrapper = styled.div`
  display: flex;

  > * {
    margin-right: 50px;
  }

  .dropDownRoot {
    height: 40px;
  }

  .placeholderClassName {
    height: 40px;
    line-height: 40px;
    padding-left: 10px;
  }

  .placeholderClassName:hover {
    cursor: pointer;
  }

  .arrowClassName {
    float: right;
  }

  .controlClassName {
    width: 150px;
    height: 40px;
    padding: 0;
  }
`;

interface HeaderPartProps {
  powtoons?: PowtoonType[];
  setCurrentPowtoonId: (id: string) => void;
  createNewPowtoon: () => void;
  currentPowtoonId?: string;
}

const initOption = { label: '', value: '' };

const HeaderPart = ({
  powtoons,
  setCurrentPowtoonId,
  createNewPowtoon,
  currentPowtoonId,
}: HeaderPartProps): JSX.Element => {
  const [powtoonOptions, setPowtoonOptions] = useState([initOption]);
  const [dropDownValue, setDropDownValue] = useState<typeof initOption>();

  useEffect(() => {
    if (powtoons && currentPowtoonId) {
      setPowtoonOptions(powtoons.map(p => ({ label: p.display_name, value: p.id })));
      const currentPowtoon = powtoons.find(p => p.id === currentPowtoonId);
      if (currentPowtoon) {
        setDropDownValue({ label: currentPowtoon.display_name, value: currentPowtoon.id });
      }
    }
  }, [powtoons, currentPowtoonId]);

  const powtoonChange = (arg: Option): void => {
    setCurrentPowtoonId(arg.value);
  };

  return (
    <HeaderWrapper>
      <span className="headerTitle">Header</span>
      <PowtoonChooserWrapper>
        <Dropdown
          className="dropDownRoot"
          arrowClassName="arrowClassName"
          controlClassName="controlClassName"
          placeholderClassName="placeholderClassName"
          options={powtoonOptions}
          onChange={powtoonChange}
          value={dropDownValue}
          placeholder="Select an option"
        />
        <AddButton onClick={createNewPowtoon}>Add</AddButton>
      </PowtoonChooserWrapper>
    </HeaderWrapper>
  );
};

export default HeaderPart;
