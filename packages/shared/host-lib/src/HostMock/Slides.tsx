import React, { useState } from 'react';
import { Tooltip, Button as _Button } from 'antd';
import styled from 'styled-components';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { PlusOutlined } from '@ant-design/icons';

import { Slide } from './recoils';
import NewSlideModal from './NewSlideModal';
import SlideCard from './Slide';
import { secToMillisec } from './utils';

const Root = styled.div`
  color: white;
  display: flex;
  flex-direction: column;
`;

const Button = styled(_Button)`
  align-self: center;
`;

const reorderList = (list: Array<any>, startIndex: number, endIndex: number) => {
  const result = [...list];
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const DEFAULT_DURATION = 10;

export default function Slides({
  slides,
  deleteSlide,
  createSlide,
  onSlideClick,
  onSlidesOrderChange,
  currentSlideId,
}: {
  slides: Slide[];
  deleteSlide: (slide: Slide) => void;
  createSlide: (slide: Slide) => void;
  onSlideClick: (slide: Slide) => void;
  onSlidesOrderChange: (props: {
    oldPosition: number;
    newPosition: number;
    slideId: string;
    reorderedSlides: Array<Slide>;
  }) => void;
  currentSlideId?: string;
}) {
  const [isModalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('');
  const [duration, setDuration] = useState(DEFAULT_DURATION);

  return (
    <Root>
      <Tooltip title="Add new slide">
        <Button type="primary" shape="circle" onClick={() => setModalVisible(true)} data-cy="add-new-slide-button">
          <PlusOutlined />
        </Button>
      </Tooltip>
      <DragDropContext
        onDragEnd={result => {
          if (!result.destination) {
            return;
          }
          onSlidesOrderChange({
            oldPosition: result.source.index,
            newPosition: result.destination.index,
            slideId: slides[result.source.index].id,
            reorderedSlides: reorderList(slides, result.source.index, result.destination.index),
          });
        }}
      >
        <Droppable droppableId="droppable">
          {provided => (
            <div {...provided.droppableProps} ref={provided.innerRef} data-cy="slides">
              {slides.map((s, i) => (
                <Draggable key={s.id} index={i} draggableId={s.id}>
                  {provided => (
                    <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                      <SlideCard
                        isActive={currentSlideId === s.id}
                        slide={s}
                        onDelete={() => deleteSlide(s)}
                        onSlideClick={onSlideClick}
                      />
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>

      <NewSlideModal
        visible={isModalVisible}
        onCancel={() => setModalVisible(false)}
        duration={duration}
        name={name}
        onDurationChange={setDuration}
        onNameChange={setName}
        onOk={() => {
          const newSlide = { display_name: name, duration_millisec: secToMillisec(duration) } as Slide;
          createSlide(newSlide);
          setName('');
          setDuration(DEFAULT_DURATION);
          setModalVisible(false);
        }}
      />
    </Root>
  );
}
