import { useMutation, gql } from '@apollo/client';

import { slideGqlFields } from './utils';

const useSlideMutations = (powtoonId: string, currentSlideId?: string) => {
  const [
    mutateSlideDurationOnServer,
    { loading: mutateSlideDurationLoading, error: mutateSlideDurationError },
  ] = useMutation<Record<string, any>>(
    gql`
      mutation($slideId: String!, $duration: Int!) {
        update_slides(where: { id: { _eq: $slideId } }, _set: { duration_millisec: $duration }) {
          returning {
            ${slideGqlFields}
          }
        }
      }
    `,
    {
      variables: {
        slideId: currentSlideId,
        powtoonId,
      },
    }
  );

  const [mutateSlidesOrderOnServer, { loading: mutateSlidesOrderLoading, error: mutateSlidesOrderError }] = useMutation(
    gql`
      mutation($newPos: Int!, $powtoonId: String!, $slideId: String!, $range: [Int!]!, $incValue: Int!) {
        update_slides(
          where: {
            _and: [{ position: { _in: $range } }, { powtoon_id: { _eq: $powtoonId } }, { id: { _neq: $slideId } }]
          }
          _inc: { position: $incValue }
        ) {
          returning {
            ${slideGqlFields}
          }
        }
        update_current_slide: update_slides(where: { id: { _eq: $slideId } }, _set: { position: $newPos }) {
          returning {
            ${slideGqlFields}
          }
        }
      }
    `
  );

  const [deleteSlideOnServer, { loading: deleteLoading, error: deleteError }] = useMutation(
    gql`
      mutation($slideId: String!, $slidePos: Int!) {
        delete_slides(where: { id: { _eq: $slideId } }) {
          affected_rows
        }
        update_slides(where: { position: { _gt: $slidePos } }, _inc: { position: -1 }) {
          affected_rows
        }
      }
    `
  );

  return {
    deleteSlideOnServer,
    mutateSlidesOrderOnServer,
    mutateSlideDurationOnServer,
    loading: mutateSlideDurationLoading || mutateSlidesOrderLoading || deleteLoading,
    error: mutateSlideDurationError || mutateSlidesOrderError || deleteError,
  };
};

export default useSlideMutations;
