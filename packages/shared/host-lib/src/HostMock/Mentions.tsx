export interface Mention {
  [key: string]: { firstName: string; lastName: string; thumbUrl?: string };
}
