import React, { useEffect, useRef, useState } from 'react';
import { RecoilRoot } from 'recoil';
import {
  ApolloProvider,
  ApolloClient,
  InMemoryCache,
  useQuery,
  gql,
  HttpLink,
  split,
  useSubscription,
  useMutation,
} from '@apollo/client';
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';
import { useRecoilState } from 'recoil';
import { pick } from 'lodash';
import {
  NotificationDescriptor,
  PowtoonStudioHost,
  PowtoonStudioHostProps,
  PowtoonStudioHostRef,
  PowtoonUser,
  ZoidCoreManifest,
} from '@powtoon/plugin-lib';

import { Mention } from './Mentions';
import { slidesState, currentSlideState, currentPowtoonIdState, Slide, Powtoon as PowtoonType } from './recoils';
import Powtoon from './Powtoon';
import { apolloClientUri, apolloWsUri } from '../config';
import { slideGqlFields, createDefaultSlide, patchSlidesWithBadges, secToMillisec } from './utils';
import { getToken } from './jwt';
const wsLink = new WebSocketLink({
  uri: apolloWsUri,
  options: {
    reconnect: true,
  },
});

const httpLink = new HttpLink({
  uri: apolloClientUri,
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
  },
  wsLink,
  httpLink
);

const client = new ApolloClient({
  link: splitLink,
  cache: new InMemoryCache(),
  connectToDevTools: process.env.NODE_ENV !== 'production',
});

// TODO: simplify. no need for manifest etc. just the params
const manifest = new (class Manifest extends ZoidCoreManifest {
  tag = 'powtoon-comments-plugin';
  url = new URL(location.href).origin; // this is just for demo
})();

const ONE_SEC_IN_MILLISEC = 1000;

function Content() {
  const [currentPowtoonId, setCurrentPowtoonId] = useRecoilState(currentPowtoonIdState);
  const [currentSlide, setCurrentSlide] = useRecoilState(currentSlideState);
  const [slides, setSlides] = useRecoilState(slidesState);
  const [pluginIsReady, setPluginIsReady] = useState<boolean>(false);
  const pluginRef = useRef<PowtoonStudioHostRef>(null);

  const { data, refetch } = useQuery(
    gql`
      query {
        powtoons {
          display_name
          id
          slides_aggregate(order_by: { position: asc }) {
            nodes {
              ${slideGqlFields}
            }
          }
        }
      }
    `
  );

  const [insertPowtoon] = useMutation(
    gql`
      mutation ($object: powtoons_insert_input!) {
        insert_powtoons_one(object: $object) {
          id
          display_name
        }
      }
    `
  );

  const { data: slidesData, loading: slidesLoading } = useSubscription(
    gql`
      subscription {
        slides_aggregate(where: { powtoon_id: { _eq: "${currentPowtoonId}" } }, order_by: { position: asc }) {
          nodes {
            ${slideGqlFields}
          }
        }
      }
    `,
    {
      skip: !currentPowtoonId,
    }
  );

  const [insertSlide] = useMutation(
    gql`
      mutation($slide: slides_insert_input!) {
        insert_slides(objects: [$slide]) {
          returning {
            ${slideGqlFields}
          }
        }
      }
    `
  );

  const addSlide = (slide: Slide, newPowtoonId?: string) =>
    insertSlide({
      variables: {
        slide: {
          ...slide,
          powtoon_id: newPowtoonId || currentPowtoonId,
          position: slides.length,
        },
      },
    });

  const notifyOnSlidesChange = (newSlides: Slide[]) => {
    if (currentPowtoonId) {
      pluginRef.current?.notifyModelEvent({
        name: 'slides',
        slides: newSlides.map(slide => pick(slide, ['id', 'duration_millisec', 'position'])),
      });
    }
  };

  useEffect(() => {
    pluginRef.current?.notifyUiEvent({ name: 'initialize' });
    return () => pluginRef.current?.notifyUiEvent({ name: 'shutdown' });
  }, []);

  useEffect(() => {
    if (currentSlide?.id) {
      pluginRef.current?.notifyUiEvent({ name: 'selection', target: { type: 'slideThumb', id: currentSlide?.id } });
    }
  }, [currentSlide?.id]);

  const onNotifyUsers: PowtoonStudioHostProps['onNotifyUsers'] = (event: NotificationDescriptor) => {
    window.console.log('onNotifyUsers', event);
  };

  const onSetBadge: PowtoonStudioHostProps['onSetBadge'] = ({ target, badge }) => {
    // eslint-disable-next-line no-console
    // console.log({ target, badge });

    setSlides(oldSlides => {
      const slideIdx = oldSlides.findIndex(({ id }) => id === target.id);
      if (slideIdx > -1) {
        const slidesCopy = [...oldSlides];
        slidesCopy[slideIdx] = {
          ...slidesCopy[slideIdx],
          badges: parseFloat(badge.data),
        };
        return slidesCopy;
      }

      return oldSlides;
    });
  };

  const onSetMenu: PowtoonStudioHostProps['onSetMenu'] = ({ target, menu }) => {
    // eslint-disable-next-line no-console
    target;
    menu;
  };

  useEffect(() => {
    if (!pluginIsReady) {
      // plugin isn't ready to receive events, wait for it.
      return;
    }

    let currentPowtoon;
    if (currentPowtoonId) {
      currentPowtoon = data?.powtoons?.find((p: PowtoonType) => p.id === currentPowtoonId);
      if (currentPowtoon) {
        pluginRef.current?.notifyModelEvent({
          name: 'ready',
          powtoonId: currentPowtoon.id,
          userId: '1',
        });
      }
    } else {
      currentPowtoon = data?.powtoons?.[0];
      if (currentPowtoon) {
        setCurrentPowtoonId(currentPowtoon.id);
      }
    }
  }, [data, currentPowtoonId, pluginIsReady]);

  useEffect(() => {
    const newSlides = slidesData?.slides_aggregate?.nodes;
    if (newSlides) {
      if (!newSlides.length) {
        addSlide(createDefaultSlide());
        return;
      }

      const patchedSlides = patchSlidesWithBadges(slides, newSlides);

      notifyOnSlidesChange(patchedSlides);
      setSlides(patchedSlides);
    }
  }, [slidesData?.slides_aggregate?.nodes]);

  const createNewPowtoon = async (): Promise<void> => {
    try {
      const resp = await insertPowtoon({
        variables: {
          object: {
            created_at: new Date().toISOString(),
            display_name: `Powtoon ${data?.powtoons.length + 1 || ''}`,
          },
        },
      });
      setCurrentPowtoonId(resp.data.insert_powtoons_one.id);
      refetch();
    } catch (err) {}
  };

  const onReady: PowtoonStudioHostProps['onReady'] = () => {
    setPluginIsReady(true);
  };

  const getPowtoonUsers = (): Promise<{ [key: string]: PowtoonUser }> => {
    // raw data object format (for demo purposes only)
    const mention_raw_list: Mention = {
      '1': {
        firstName: 'Nir',
        lastName: 'Orlev',
        thumbUrl: 'http://2.gravatar.com/avatar/2cfff149a0e2a21acd2379072b2cd5c3',
      },
      '2': {
        firstName: 'Sven',
        lastName: 'Hoffman',
        thumbUrl: 'https://secure.gravatar.com/avatar/bcc404eb64c0e83b90a5279b0799e479',
      },
      '3': {
        firstName: 'Alexander',
        lastName: 'Martynenko',
        thumbUrl: 'https://secure.gravatar.com/avatar/30ee22c1828c281bf8cb6bad4d1d1764',
      },
      '4': {
        firstName: 'Dani',
        lastName: 'Kenan',
        thumbUrl: 'https://secure.gravatar.com/avatar/075578f26aaebcebc997a7cfe25819a4',
      },
      '5': {
        firstName: 'Eugenia',
        lastName: 'Pugachova',
        thumbUrl: 'https://secure.gravatar.com/avatar/cefa2b0eaa874679b9ec92a576ac97f3',
      },
      '6': {
        firstName: 'Alex',
        lastName: 'Sahno',
        thumbUrl: 'https://secure.gravatar.com/avatar/16258b76521e950a25ef8410e1a0304a',
      },
    };

    return Promise.resolve(
      Object.keys(mention_raw_list).reduce<{ [key: string]: any }>((acc, id) => {
        acc[id] = {
          id: id,
          thumbUrl: mention_raw_list[id].thumbUrl,
          firstName: mention_raw_list[id].firstName,
          lastName: mention_raw_list[id].lastName,
        };
        return acc;
      }, {})
    );
  };

  const getUsers = (users: [string]): Promise<{ [key: string]: PowtoonUser }> => {
    const powtoonUsers: Mention = {
      '1': {
        firstName: 'Nir',
        lastName: 'Orlev',
        thumbUrl: 'http://2.gravatar.com/avatar/2cfff149a0e2a21acd2379072b2cd5c3',
      },
      '2': {
        firstName: 'Sven',
        lastName: 'Hoffman',
        thumbUrl: 'https://secure.gravatar.com/avatar/bcc404eb64c0e83b90a5279b0799e479',
      },
      '3': {
        firstName: 'Alexander',
        lastName: 'Martynenko',
        thumbUrl: 'https://secure.gravatar.com/avatar/30ee22c1828c281bf8cb6bad4d1d1764',
      },
      '4': {
        firstName: 'Dani',
        lastName: 'Kenan',
        thumbUrl: 'https://secure.gravatar.com/avatar/075578f26aaebcebc997a7cfe25819a4',
      },
      '5': {
        firstName: 'Eugenia',
        lastName: 'Pugachova',
        thumbUrl: 'https://secure.gravatar.com/avatar/cefa2b0eaa874679b9ec92a576ac97f3',
      },
      '6': {
        firstName: 'Alex',
        lastName: 'Sahno',
        thumbUrl: 'https://secure.gravatar.com/avatar/16258b76521e950a25ef8410e1a0304a',
      },
      'Powtoon User': {
        firstName: 'Powtoon User',
        lastName: '',
      },
      testUser: {
        thumbUrl: '',
        firstName: 'Test User',
        lastName: '',
      },
    };

    return Promise.resolve(
      users.reduce<{ [key: string]: any }>((acc, id) => {
        if (powtoonUsers[id.trim()]) {
          acc[id.trim()] = {
            id: id.trim(),
            thumbUrl: powtoonUsers[id].thumbUrl,
            firstName: powtoonUsers[id].firstName,
            lastName: powtoonUsers[id].lastName,
          };
        }
        return acc;
      }, {})
    );
  };

  const onGetDataRequest = async (methodName: string, _arg: any): Promise<any> => {
    // console.log(JSON.stringify({ call: 'onGetDataRequest', methodName, _arg }));
    if (methodName == 'getSlides') {
      return Promise.resolve({ powtoonId: currentPowtoonId, slides: slides });
    }
    if (methodName == 'getPowtoonUsers') {
      return getPowtoonUsers();
    }
    if (methodName == 'getPowtoonAccessToken') {
      return Promise.resolve(getToken());
    }

    if (methodName == 'getUsersById') {
      const users = getUsers(_arg as [string]);
      return users;
    }

    if (methodName == 'getPlayheadPosition') {
      return Promise.resolve({
        powtoonId: currentPowtoonId,
        slideId: currentSlide?.id,
        millisec: currentSlide?.duration_millisec,
      });
    }
  };

  return (
    <Powtoon
      slides={slides}
      powtoons={data?.powtoons as PowtoonType[]}
      currentSlide={currentSlide}
      currentPowtoonId={currentPowtoonId}
      setCurrentPowtoonId={setCurrentPowtoonId}
      createNewPowtoon={createNewPowtoon}
      loading={slidesLoading}
      setCurrentSlide={setCurrentSlide}
      // needed to decrease ui update time
      setReorderedSlides={setSlides}
      addSlide={addSlide}
      onTimelineChange={time => pluginRef.current?.notifyUiEvent({ name: 'timeline', millisec: secToMillisec(time) })}
      onDurationChange={(action, newStartSeconds) =>
        pluginRef.current?.notifyModelEvent({
          name: 'slideTime',
          id: currentSlide?.id as string,
          durationMillisec: action === 'add' ? ONE_SEC_IN_MILLISEC : -ONE_SEC_IN_MILLISEC,
          action,
          startMillisec: secToMillisec(newStartSeconds),
        })
      }
    >
      <PowtoonStudioHost
        manifest={manifest}
        ref={pluginRef}
        onSetBadge={onSetBadge}
        onSetMenu={onSetMenu}
        onReady={onReady}
        onGetDataRequest={onGetDataRequest}
        onOpenShareDialog={() => {
          window.console.log('onOpenShareDialog called');
        }}
        onNotifyUsers={onNotifyUsers}
        onPause={() => {
          window.console.log('onPause called');
        }}
        onNavigateToTarget={({ target }) => {
          window.console.log('onNavigateToTarget called', JSON.stringify(target));
        }}
      />
    </Powtoon>
  );
}

export default function HostMock() {
  return (
    <ApolloProvider client={client}>
      <RecoilRoot>
        <Content />
      </RecoilRoot>
    </ApolloProvider>
  );
}
