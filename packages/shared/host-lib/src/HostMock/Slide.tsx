import React from 'react';
import { Card as _Card, Badge as _Badge } from 'antd';
import styled from 'styled-components';
import { DeleteOutlined } from '@ant-design/icons';

import { Slide } from './recoils';
import { millisecToSec } from './utils';

const Card = styled(_Card)<{ $isActive: boolean }>`
  margin-bottom: 20px;
  position: relative;
  font-size: 0.9em;
  transform: ${({ $isActive }) => ($isActive ? 'scale(1.1)' : 'scale(1)')};
  transition: transform 0.3s;
  & .ant-card-body {
    padding-top: 10px;
    padding-bottom: 10px;
    p {
      margin-bottom: 5px;
    }
  }
  &:first-of-type {
    margin-top: 20px;
  }
  &:hover {
    transform: scale(1.1);
  }
`;

const DeleteSlideButton = styled.button`
  background-color: inherit;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  border: none;
  width: 20px;
  height: 20px;
  position: absolute;
  bottom: 3px;
  left: 3px;
`;

const Badge = styled(_Badge)`
  position: absolute;
  top: -8px;
  right: -8px;
`;

export default function SlideCard({
  slide,
  onDelete,
  onSlideClick,
  isActive,
}: {
  slide: Slide;
  onDelete: () => void;
  onSlideClick: (slide: Slide) => void;
  isActive: boolean;
}) {
  return (
    <Card key={slide.id} onClick={() => onSlideClick(slide)} $isActive={isActive}>
      <p>
        <b data-cy="slide-name">{slide.display_name}</b>
      </p>
      <p>{millisecToSec(slide.duration_millisec)} sec</p>
      <DeleteSlideButton
        onClick={evt => {
          evt.stopPropagation();
          onDelete();
        }}
        data-cy="delete-slide-button"
      >
        <DeleteOutlined />
      </DeleteSlideButton>
      {slide.badges && <Badge count={slide.badges} />}
    </Card>
  );
}
