# powtoon-comments-mono

This is the monorepo for the *powtoon** commenting system. It contains all the code, artifacts and tools needed to implement the **Commenting System** external system/plugin for powtoon.

## Product

https://www.notion.so/Review-Feature-PRD-for-Welldone-260f9f9049484be1bc4124ca349de805

Also contains ax xd

Comment Panel XD (not final):

https://xd.adobe.com/view/70f4150f-f5ae-448f-a162-5bdcedada444-25a7/

## Development

### Overview

#### A Typescript Monorepo
This is a `typescript` cross layer monorepo. Almost everything is in typescript, not just the frontend and backend code, but also command line tools and even the devops.

#### Yarn (v.1) workspaces and lerna

We rely mainly on `yarn workspaces`. One can usually just execute a `yarn install` at the monorepo root and that will bootstrap everything.

We only use `lerna` if several packages need to be published to an npm repository and/or need cross-package version management.


#### Typescript without babel

We rely on direct `typescript` compilation and its plugins without using `babel` whenever possible. This is in order to avoid multiple layers of configuration and keep things a bit simpler.

#### Usage of tsc, webpack, ts-node and ts-node-dev, ts-jest

We rarely compile typescript code using `tsc` directly.

`webpack` is usually used for packaging production code and as the dev server of frontend projects.

During non-frontend development we execute everything using `ts-node-dev` and for scripts and tools not running in production we use `ts-node` as their executor, such that `ts` scripts run without compilation. 

Same goes for testing. We use `ts-jest` such that `ts` scripts run without pre compilation. 

#### eslint and prettier

All packages share the same `eslint` and `prettier` configurations found at the monorepo root.

Note that we treat `eslint` as the main tool, while `prettier` is just an extension and a rule set inside `eslint`.

We rely on `eslint` for more than just linting. We use its `--fix` flag for for formatting the code (this includes the `prettier` rules), protecting quality before committing and before pushing code. Usually we integrate it with the IDE such that we let it formats our code, automatically on save, without any manual formatting.

In addition to formatting and auto-fixable errors, we use `eslint` as the sole code quality tool in our IDEs such that we have a single source of truth for code quality rules.

Overriding the top level `eslint` (extension for project types, rule overrides, package specific ignore patterns etc.) can be made at package or even directory level by adding an extending `.eslintrc` file at that level.


We use the same 
### IDE - VSCode

This project is optimized for `vscode` usage. Required extensions and settings are baked in and ensure you have a smooth and easy development experience where all the tools play together well.

When prompted, accept the recommended extensions.

As the project evolves, more tools, extension and setting will be added. These will all be optimized for `vscode` as well. Usage of other IDEs is possible, but will required much more maintenance and configuration work on your side and/or provide a lesser experience.

### Monorepo Structure

The `packages` is split by layer (`front`, `back` etc.) 

Inside each layer there may be `apps` and `libs`.

`apps` contain externalized packages that are used as entry points outside the layer, e.g. deployable services, sites, command line tools and even libraries which are deployable and used outside our monorepo.

`libs` contain packages that are only used within the layer containing them, e.g a ui design system library in `front`, an orm library in `back` etc. 
These libs maybe have been separated from their `apps` since they are shared between multiple apps within the same layer, or even libs which are separated just for clarity, decoupled functionality etc. 

The top level (layer level) `shared` folder is a bit special, since it is not a layer, but rather a cross-layer container. 
It contain packages with code that is shared across multiple layers. For example data models and types common to both `front` and `back` services code. The `apps` may contain scripts and extensions for build or deployment time, that are cross layer in nature.


```
front/
    apps/                        -- external deployable sites/libs
        customer-dashboard        
        admin-backoffice
    libs/                        -- used only by 'front' apps/libs
        uikit
        auth
back/
    apps/
        api
        jobs
    libs/
        nestjs-utils
        postgraphile-plugins
devops/
    apps/
        k8s
        aws-cdk

shared/
    apps/
        scripts
    libs/                        -- used by cross-layer apps/libs
        data-models

```
