#!/usr/bin/env bash

echo "=================================="
echo "Merge to develop started"
echo "=================================="
git fetch && git fetch --tags

git pull origin master
git checkout develop
git pull origin develop

git merge --no-ff origin/master -m "merge master into develop"

git push origin develop

echo "=================================="
echo "Merge to develop completed"
echo "=================================="