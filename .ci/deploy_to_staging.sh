#!/usr/bin/env bash

run_build() {
    curl --fail -X POST \
       -H "destination_url: https://prod-jenkins.powtoon.com/job/commenting-system-staging/build" \
       -H "destination_auth: ${JENKINS_STAGING_SYSTEM_USER_CREDENTIALS}" \
       https://prod-jenkins-proxy.powtoon.com
    rc=$?
    if [[ $rc -ne 0 ]]; then
        curl -X POST \
            -H "destination_url: https://prod-jenkins.powtoon.com/job/commenting-system-staging/build" \
            -H "destination_auth: ${JENKINS_STAGING_SYSTEM_USER_CREDENTIALS}" \
            https://prod-jenkins-proxy.powtoon.com
        export success="false"
        return $rc
    fi
}

export success="true"

run_build

if [ $success == "false" ]; then
    exit 1
fi