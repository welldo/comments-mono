#!/usr/bin/env bash

run_build() {
    curl --fail -X POST \
       -H "destination_url: https://jenkins.k8s.powtoon.com/job/commenting-system/build" \
       -H "destination_auth: ${JENKINS_DEV_SYSTEM_USER_CREDENTIALS}" \
       https://dev-jenkins-proxy.k8s.powtoon.com
    rc=$?
    if [[ $rc -ne 0 ]]; then
        curl -X POST \
            -H "destination_url: https://jenkins.k8s.powtoon.com/job/commenting-system/build" \
            -H "destination_auth: ${JENKINS_DEV_SYSTEM_USER_CREDENTIALS}" \
            https://dev-jenkins-proxy.k8s.powtoon.com
        export success="false"
        return $rc
    fi
}

export success="true"

run_build

if [ $success == "false" ]; then
    exit 1
fi